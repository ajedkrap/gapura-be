const { seq } = require("../../configs/sequelize")

const getArea = require("../area/getArea")
const { Client, ClientDetail, ClientDocument, Account } = require("../objects")

module.exports = async (data = {}) => {
  try {
    const result = await seq.transaction(async (trans) => {
      data.where = { ...data.where || null, status: 'active' }
      const userClients = await Client.findAll({ ...data, transaction: trans })
      const clientData = await Promise.all(userClients.map(async (value) => {
        const { id_client } = value
        const details = await ClientDetail.findOne({ where: { id_client }, transaction: trans }).then(value => value.dataValues).catch(err => { throw new Error(err) })
        const documents = await ClientDocument.findOne({ where: { id_client }, transaction: trans }).then(value => value.dataValues).catch(err => { throw new Error(err) })
        const accounts = await Account.findOne({ where: { id_client }, transaction: trans }).then(value => value.dataValues).catch(err => { throw new Error(err) })
        // const area = await getArea(details.area_code, { transaction: trans })
        // details = { ...details, area }
        return {
          client: value, details, documents, accounts
        }
      }))
      return { clients: clientData }
    })
    return result
  }
  catch (e) {
    throw new Error(e.message)
  }
}