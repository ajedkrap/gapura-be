const { Client } = require("../objects")

module.exports = async (data) => {
  data.where = { ...data.where, status: 'active' }
  return Client.count({ ...data })
}