const { Client } = require("../objects")

module.exports = async (data) => Client.count({ ...data })