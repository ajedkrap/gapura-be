// const db = require("../../configs/database")
require('dotenv').config()
const { DB_NAME_DEV } = process.env
const { seq } = require("../../configs/sequelize")
const { Client, ClientDetail } = require("../objects")

const sqlincrement = `SELECT SUM(\`auto_increment\`) as increment FROM INFORMATION_SCHEMA.TABLES WHERE table_name='clients' AND table_schema='${DB_NAME_DEV}'`


module.exports = async (data, reason = "") => {
  try {
    const result = await seq.transaction(async (trans) => {
      switch (reason) {
        case "register": {
          const phoneExist = await Client.findAll({ where: { phone_number: data.phone_number }, transaction: trans })
          const emailExist = await Client.findAll({ where: { email: data.email }, transaction: trans })
          const ktpExist = await ClientDetail.findAll({ where: { ktp_number: data.ktp_number }, transaction: trans })
          if (phoneExist.length > 0) return [false, "Nomor Ponsel Sudah Terdaftar"]
          else if (emailExist.length > 0) return [false, "Email Sudah Terdaftar"]
          else if (ktpExist.length > 0) return [false, "Ktp Sudah Terdaftar"]
          else {
            const clientIncrement = await seq.query(sqlincrement)
            return [true, clientIncrement[0][0].increment]
          }
        }
        case "login": {
          const phoneExist = await Client.findAll({ where: { phone_number: data.phone_number }, transaction: trans })
          if (phoneExist.length < 1) return [false, "Nomor Tidak Terdaftar"]
          else if (phoneExist.length > 0) return [true, phoneExist[0].dataValues]
          else return [false, "Login Gagal"]
        }
        default: {
          const userExist = await Client.findAll({ where: { id_client: data.id_client }, transaction: trans })
          if (userExist.length < 1) return [false, "User Tidak Ditemukan"]
          else if (userExist.length > 0) return [true, userExist[0].dataValues]
          else return [false, "Get Client Gagal"]
        }
      }
    })
    return result
  }
  catch (err) {
    throw new Error(err.message)
  }
}
// module.exports = (data, tableName) => {
//   let sql = `SELECT * FROM ${tableName} WHERE `

//   for (let i = 0; i < data.length; i++) {
//     if (i > 0) {
//       sql += "OR "
//     }
//     sql += "? "
//   }

//   return new Promise((resolve, reject) => {
//     db.query(sql, data, (err, res) => {
//       if (err) reject(new Error("Database Gagal"))
//       else if (res.length > 1) resolve(res)
//       else if (res.length > 0) resolve(res[0])
//       else resolve(false)
//     })
//   })
// }