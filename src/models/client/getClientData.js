const { seq } = require("../../configs/sequelize")
const { ClientDetail, ClientDocument, Account, Deposit, Loan } = require("../objects")

module.exports = async (data) => {
  try {
    const result = await seq.transaction(async (trans) => {
      const getData = { where: { ...data }, transaction: trans }
      const details = await ClientDetail.findAll(getData)
      const documents = await ClientDocument.findAll(getData)
      const accounts = await Account.findAll(getData)
      const deposits = await Deposit.findAll(getData)
      const loans = await Loan.findAll(getData)

      return { details, documents, accounts, deposits, loans }

    })
    return result
  }
  catch (e) {
    throw new Error(e.message)
  }
}




// const db = require("../../configs/database");
// const { rollback } = require("../../helpers/database");

// const t1 = `client_details`;
// const t2 = `client_documents`;
// const t3 = `accounts`;
// const t4 = `deposit`;
// const t5 = `loan`;

// let sql = "SELECT * FROM ; WHERE ? "
// let order = "ORDER BY date DESC"

// module.exports = (id) => {
//   return new Promise((resolve, reject) => {
//     db.beginTransaction(transErr => {
//       if (transErr) reject(new Error("Database Gagal"))
//       else {
//         db.query(sql.replace(';', t1), id, (detailError, detailResult, fields) => {
//           if (detailError) rollback(db, detailError, "Get Detail Gagal", reject);
//           else {
//             db.query(sql.replace(";", t2), id, (documentError, documentResult) => {
//               if (documentError) rollback(db, documentError, "Get Document Gagal", reject)
//               else {
//                 db.query(sql.replace(";", t3), id, (accountError, accountResult) => {
//                   if (accountError) rollback(db, accountError, "Get Account Gagal", reject)
//                   else {
//                     db.query(sql.replace(";", t4).concat(order), id, (depositError, depositResult) => {
//                       if (depositError) rollback(db, depositError, "Get Deposit Gagal", reject)
//                       else {
//                         db.query(sql.replace(";", t5).concat(order), id, (loanError, loanResult) => {
//                           if (loanError) rollback(db, loanError, "Get Loan Gagal", reject)
//                           else {
//                             db.commit(commitError => {
//                               if (commitError) rollback(db, commitError, "Get Client Data Gagal", reject)
//                               else {
//                                 resolve({
//                                   detail: detailResult[0],
//                                   document: documentResult[0],
//                                   account: accountResult[0],
//                                   deposit: depositResult,
//                                   loan: loanResult
//                                 })
//                               }
//                             })
//                           }
//                         })
//                       }
//                     })
//                   }
//                 })
//               }
//             })
//           }
//         })
//       }
//     })
//   })
// }