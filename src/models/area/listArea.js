const { Op } = require("sequelize")
const { Area } = require("../objects")


module.exports = async (areaCode) => {

  return Area.findAll({
    where: {
      area_code: {
        [Op.like]: areaCode.length > 7 ? `${areaCode}.%` : areaCode.length > 1 ? `${areaCode}.__` : '__'
      }
    }
  }).then(res => {
    if (res.length < 1) throw new Error("List Area Tidak Ditemukan")
    else return res
  }).catch(err => new Error(`Get Area List Error: ${err.message}`))
}


// const db = require("../../configs/database")

// const table = 'areas'

// module.exports = async (areaCode = "") => {
//   let sql = `SELECT * FROM ${table} WHERE area_code `


//   if (areaCode.length > 7) {
//     sql += `LIKE '${areaCode}.%'`
//   }
//   else if (areaCode.length > 1) {
//     sql += `LIKE '${areaCode}.__'`
//   }
//   else {
//     sql += `LIKE '__'`
//   }

//   return new Promise((resolve, reject) => {
//     db.query(sql, (areaErr, res) => {
//       if (areaErr) reject(new Error("Get Area List Error"))
//       else if (res.length < 1) reject(new Error("List Area Tidak Ditemukan"))
//       else resolve(res)
//     })
//   })
// }