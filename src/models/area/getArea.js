const seq = require('sequelize')
const { Area } = require("../objects")

const table = 'areas'

const selectQuery = async (code) => `(SELECT area_name FROM ${table} WHERE area_code = '${code}')`

module.exports = async (areaCode, trans = {}) => {
  const [provinsi, kota, kecamatan, kelurahan] = areaCode.split('.')
  const kodeKota = [provinsi, kota].join('.')
  const kodeCamat = [provinsi, kota, kecamatan].join('.')
  const kodeLurah = [provinsi, kota, kecamatan, kelurahan].join(".")

  return Area.findAll({
    attributes: [
      [seq.literal(`${await selectQuery(provinsi)}`), 'Provinsi'],
      [seq.literal(`${await selectQuery(kodeKota)}`), 'Kota_Kabupaten'],
      [seq.literal(`${await selectQuery(kodeCamat)}`), 'Kecamatan'],
      [seq.literal(`${await selectQuery(kodeLurah)}`), 'Kelurahan']
    ],
    ...trans
  }).then(res => {
    const data = res[0]
    const realData = data.dataValues
    for (let key in realData) {
      if (realData[key] === null) return false
    }
    return data
  }).catch(err => new Error(`Get Area Error: ${err.message}`))
}





// const db = require("../../configs/database")

// const selectQuery = async (code) => `(SELECT area_name FROM ${table} WHERE area_code = '${code}')`

// module.exports = async (areaCode) => {
//   const [provinsi, kota, kecamatan, kelurahan] = areaCode.split('.')
//   const kodeKota = [provinsi, kota].join('.')
//   const kodeCamat = [provinsi, kota, kecamatan].join('.')
//   const kodeLurah = [provinsi, kota, kecamatan, kelurahan].join(".")

//   let sql = `SELECT 
//               ${await selectQuery(provinsi)} AS Provinsi,
//               ${await selectQuery(kodeKota)} AS KotaKabupaten,
//               ${await selectQuery(kodeCamat)} AS Kecamatan,
//               ${await selectQuery(kodeLurah)} AS Kelurahan
//               FROM ${table}`

//   return new Promise((resolve, reject) => {
//     db.query(sql, (areaErr, res) => {
//       if (areaErr) reject(new Error("Database Error"))
//       else {
//         const data = res[0]
//         for (let key in data) {
//           if (data[key] === null) resolve(false)
//         }
//         resolve(data)
//       }
//     })
//   })
// }