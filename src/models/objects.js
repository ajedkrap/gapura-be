const { seq, DataTypes } = require("../configs/sequelize")

const created_at = DataTypes.DATE, updated_at = DataTypes.DATE
const date = DataTypes.DATE
const dateOnly = DataTypes.DATEONLY

const id = {
  type: DataTypes.INTEGER(11),
  autoIncrement: true,
  primaryKey: true
}

const id_client = {
  type: DataTypes.STRING(9),
  field: "id_client"
}

const int11 = {
  type: DataTypes.INTEGER(11)
}

const idString30 = {
  type: DataTypes.STRING(30)
}

const double = {
  type: DataTypes.DOUBLE.UNSIGNED,

}

const float = {
  type: DataTypes.FLOAT.UNSIGNED,
}

const name = {
  type: DataTypes.STRING(60),
  field: "name"
}

const purpose = {
  type: DataTypes.TEXT,
  field: "purpose",
  allowNull: true
}

const period_unit = {
  type: DataTypes.ENUM(['weekly', 'monthly', 'annually'])
}

const depositStatus = {
  type: DataTypes.ENUM(['inactive', 'on progress', 'locked', 'paid off', 'withdrawal']),
  field: 'status'
}

const loanStatus = {
  type: DataTypes.ENUM(['inactive', 'on loan', 'locked', 'paid off']),
  field: 'status'
}

const document = {
  type: DataTypes.STRING(80),
}

const product = {
  id,
  name: DataTypes.STRING(255),
  prefix: DataTypes.STRING(10),
  description: DataTypes.STRING(140),
  interest_rate: { ...float, defaultValue: 0 },
  interest_calculation: DataTypes.ENUM(['fixed', 'recurring']),
  compound: DataTypes.ENUM(['daily', 'monthly']),
  posting: DataTypes.ENUM(['monthly', 'quarterly', 'annually']),
  digit_after_decimal:
  {
    ...int11,
    defaultValue: 0,
    field: "digit_after_decimal"
  },
  in_multiple_of: {
    type: DataTypes.ENUM(['100', '1000', '10000']),
    field: "in_multiple_of"
  },
  lock_in_value: { ...int11, defaultValue: 0, field: "lock_in_value" },
  lock_in_period: {
    type: DataTypes.ENUM(['monthly', 'annually']),
    field: "lock_in_period"
  },
  tax: DataTypes.STRING(60),
  theme_color: {
    type: DataTypes.STRING(10),
    defaultValue: 'E3E3E3'
  },
  created_by: { ...idString30, field: "created_by" },
  created_at,
  updated_at
}

const record = {
  id,
  tenor: { ...int11, defaultValue: 1 },
  penalty: { ...double, defaultValue: 0 },
  status: DataTypes.ENUM(['waiting', 'paid', 'locked']),
  paid_date: {
    type: dateOnly,
    allowNull: true
  },
  due_date: dateOnly,
  created_at,
  updated_at
}

const _application = {
  id_application: { ...idString30, field: "id_application" },
  id_product: { ...idString30, field: "id_product" },
  id_client: DataTypes.STRING(11),
  name,
  purpose,
  amount: { ...double, defaultValue: 0 },
  tenor: { ...int11, defaultValue: 1 },
  period_unit,
  status: DataTypes.ENUM(['applied', 'in progress', 'followed up', 'rejected', 'approved']),
  id_collateral: {
    ...idString30,
    allowNull: true
  },
  reviewed_by: {
    type: DataTypes.STRING(40),
    allowNull: true
  },
  surveyed_by: {
    type: DataTypes.STRING(40),
    allowNull: true
  },
  approved_by: {
    type: DataTypes.STRING(40),
    allowNull: true
  },
  survey_date: {
    type: DataTypes.STRING(40),
    allowNull: true
  },
  message: DataTypes.TEXT,
  created_at,
  updated_at
}

const _mutation = {
  id,
  id_account: {
    type: DataTypes.STRING(15),
    field: 'id_account'
  },
  entry: DataTypes.ENUM(['debit', 'credit']),
  amount: { ...double, defaultValue: 0 },
  new_balance: { ...double, defaultValue: 0 },
  transaction: DataTypes.ENUM(['topup', 'withdrawal', 'transfer', 'payment', 'deposit', 'loans', 'koperasi']),
  message: DataTypes.STRING(80),
  note: DataTypes.STRING(80),
  created_at,
  updated_at
}

const Client = seq.define('client', {
  id,
  id_client,
  email: {
    type: DataTypes.STRING(80),
    field: 'email'
  },
  full_name: {
    type: DataTypes.STRING(30),
    field: 'full_name'
  },
  phone_number: {
    type: DataTypes.STRING(14),
    field: 'phone_number'
  },
  pin: {
    type: DataTypes.STRING(30),
    field: 'pin'
  },
  status: {
    type: DataTypes.ENUM(['active', 'inactive']),
    field: 'status'
  },
  entry_date: DataTypes.DATEONLY,
  created_at,
  updated_at
}, {
  tableName: "clients"
})

const ClientDetail = seq.define('client_detail', {
  id,
  id_client,
  ktp_number: DataTypes.STRING(20),
  marital_status: {
    type: DataTypes.ENUM(['married', 'single', 'widow']),
  },
  address: {
    type: DataTypes.STRING(80),
  },
  area_code: {
    type: DataTypes.STRING(13),
  },
  birthplace: {
    type: DataTypes.STRING(80),
  },
  birth_date: {
    type: DataTypes.DATEONLY
  },
  profession: {
    type: DataTypes.STRING(60),
  },
  npwp: {
    type: DataTypes.STRING(20),
    allowNull: true
  },
  bank_account: {
    type: DataTypes.STRING(25),
    allowNull: true
  },
  gender: {
    type: DataTypes.ENUM(['male', 'female']),
  },
  mother_name: {
    type: DataTypes.STRING(25),
  },
  created_at,
  updated_at
}, {
  tableName: 'client_details'
})

const ClientDocument = seq.define('client_document', {
  id,
  id_client,
  ktp: { ...document, allowNull: true },
  picture: { ...document, allowNull: true },
  signature: { ...document, allowNull: true },
  npwp: { ...document, allowNull: true },
  created_at,
  updated_at
}, {
  tableName: 'client_documents'
})

const Account = seq.define('account', {
  id,
  id_account: {
    type: DataTypes.STRING(15),
    field: 'id_account'
  },
  id_client,
  balance: { ...double, defaultValue: 0, field: 'balance' },
  wajib: { ...double, defaultValue: 0, field: 'wajib' },
  pokok: { ...double, defaultValue: 0, field: 'pokok' },
  wajib_month: { ...int11, defaultValue: 0, field: 'wajib_month' },
  created_at,
  updated_at
}, {
  tableName: 'accounts'
})

const Deposit = seq.define('deposit', {
  id,
  id_deposit: { ...idString30, field: "id_deposit" },
  id_deposit_prod: { ...idString30, field: "id_deposit_prod" },
  id_application: { ...idString30, field: "id_application" },
  id_client: { type: DataTypes.STRING(11) },
  name,
  purpose,
  amount: { ...double, defaultValue: 0 },
  tenor: { ...int11, defaultValue: 1 },
  period_unit,
  interest: { ...double, defaultValue: 0 },
  tax: { ...float, defaultValue: 0, allowNull: true },
  installment_amount: { ...double, defaultValue: 0 },
  installment_remaining: { ...int11, defaultValue: 0 },
  deposit_remaining: { ...double, defaultValue: 0 },
  status: depositStatus,
  approved_date: date,
  due_date: date,
  created_at,
  updated_at
}, {
  tableName: 'deposits'
})

const Loan = seq.define('loan', {
  id,
  id_loan: { ...idString30, field: "id_loan" },
  id_loan_prod: { ...idString30, field: "id_loan_prod" },
  id_application: { ...idString30, field: "id_application" },
  id_client: { type: DataTypes.STRING(11), field: "id_client" },
  name,
  purpose,
  amount: { ...double, defaultValue: 0 },
  tenor: { ...int11, defaultValue: 1 },
  period_unit,
  interest: { ...double, defaultValue: 0 },
  repayment_amount: { ...double, defaultValue: 0 },
  repayment_remaining: { ...int11, defaultValue: 0 },
  loan_remaining: { ...double, defaultValue: 0 },
  status: loanStatus,
  approved_date: date,
  due_date: date,
  created_at,
  updated_at
}, {
  tableName: 'loans'
})

const DepositProduct = seq.define('deposit_product', {
  ...product,
  id_deposit_prod: { ...idString30, field: "id_deposit_prod" },
  deposit_type: DataTypes.ENUM(['fixed', 'recurring']),
}, {
  tableName: 'deposit_products'
})

const LoanProduct = seq.define('loan_product', {
  ...product,
  id_loan_prod: { ...idString30, field: "id_loan_prod" },
  with_collateral: DataTypes.TINYINT,
}, {
  tableName: 'loan_products'
})

const DepositRecord = seq.define('deposit_record', {
  ...record,
  installment_amount: { ...double, defaultValue: 0 },
}, {
  tableName: 'deposit_records'
})

const LoanRecord = seq.define('loan_record', {
  ...record,
  repayment_amount: { ...double, defaultValue: 0 }
}, {
  tableName: 'loan_records'
})

const Application = seq.define('application', {
  id,
  ..._application
}, {
  tableName: 'applications'
})

const Collateral = seq.define('collateral', {
  id_collateral: { ...idString30, field: 'id_collateral' },
  id_application: { ...idString30, field: 'id_application' },
  id_loan: { ...idString30, field: 'id_loan' },
  type: {
    type: DataTypes.ENUM(['property', 'vehicle']),
    field: "type"
  },
  photo: {
    type: DataTypes.STRING(80),
    field: "photo"
  },
  details: {
    type: DataTypes.STRING(80),
    field: "details"
  },
  created_at,
  updated_at
}, {
  tableName: 'collaterals'
})

const Officer = seq.define('officer', {
  id,
  id_officer: DataTypes.STRING(20),
  full_name: DataTypes.STRING(80),
  email: DataTypes.STRING(40),
  password: DataTypes.STRING(60),
  address: DataTypes.TEXT,
  photo: {
    type: DataTypes.STRING(40),
    allowNull: true
  },
  role: DataTypes.ENUM(['supervisor', 'accountant', 'finance', 'operational', 'hr']),
  created_at,
  updated_at
}, {
  tableName: 'officers'
})

const Mutation = seq.define('mutation', {
  ..._mutation
}, {
  tableName: 'mutations'
})

const Penalty = seq.define('penalty', {
  id,
  id_client,
  id_account: {
    type: DataTypes.STRING(15),
    field: 'id_account'
  },
  reason: {
    type: DataTypes.STRING(80),
    field: 'reason'
  },
  amount: {
    type: DataTypes.DOUBLE,
    field: 'amount'
  },
  log: {
    type: DataTypes.STRING(80),
    field: 'log'
  },
  created_at,
  updated_at
}, {
  tableName: 'penalties'
})

const Area = seq.define('area', {
  area_code: {
    type: DataTypes.STRING(13),
    primaryKey: true
  },
  area_name: DataTypes.STRING(25)

}, {
  tableName: 'areas',
  freezeTableName: true
})

const Holiday = seq.define('holiday', {
  id,
  name: DataTypes.STRING(80),
  description: DataTypes.TEXT,
  date: dateOnly,
  created_at,
  updated_at
}, {
  tableName: 'holidays'
})

module.exports = {
  Client,
  ClientDetail,
  ClientDocument,
  Account,
  Officer,
  Deposit,
  DepositProduct,
  DepositRecord,
  Loan,
  LoanProduct,
  LoanRecord,
  Application,
  Collateral,
  Area,
  Mutation,
  Penalty,
  Holiday
}