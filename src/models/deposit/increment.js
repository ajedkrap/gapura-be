require('dotenv').config()
const { DB_NAME_DEV } = process.env
const { seq } = require('../../configs/sequelize')

const sqlincrement = `SELECT SUM(\`auto_increment\`) as increment FROM INFORMATION_SCHEMA.TABLES WHERE table_name='deposits' AND table_schema='${DB_NAME_DEV}'`

module.exports = async () =>
  seq.query(sqlincrement)
    .then(value => {
      const increment = value[0][0].increment
      return increment === null ? 1 : increment
    })
    .catch(err => new Error(err.message))
