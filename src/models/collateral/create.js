const { Collateral } = require("../objects")

module.exports = async (data) => Collateral.create({ ...data })