require('dotenv').config();
const { APP_HOST } = process.env;
// const os = require("os");
// const ip = os.networkInterfaces().en0[1].address;
// const host = `http://${ip}:${APP_PORT}`

const { Officer } = require("../objects")

module.exports = async (data) => {
  try {
    const newOfficer = await Officer.create({ ...data })
    newOfficer.photo = APP_HOST + "/officers/photo/" + newOfficer.photo
    return newOfficer.dataValues
  }
  catch (err) {
    return new Error(err.message)
  }
}

// const { commit } = require("../../configs/database");
// const db = require("../../configs/database");
// const { rollback } = require("../../helpers/database")



// let tableName = `officers`
// let sql = `INSERT INTO ${tableName} SET ?`

  // new Promise((resolve, reject) => {
  //   db.beginTransaction(transErr => {
  //     if (transErr) rollback(db, transErr, 'Database Gagal', reject)
  //     else {
  //       db.query(sql, data, (registerErr, registerRes) => {
  //         if (registerErr) rollback(db, registerErr, 'Insert Officer Gagal', reject)
  //         else if (registerRes.affectedRows < 1) reject( new Error('Insert Officer Gagal'))
  //         else {
  //           db.commit(commitErr => {
  //             if (commitErr) rollback(db, registerErr, 'Daftar Officer Gagal', reject)
  //             else {
  //               data.photo = host + "/officers/photo/" + data.photo
  //               resolve(data)
  //             }
  //           })
  //         }
  //       })
  //     }
  //   })
  // })