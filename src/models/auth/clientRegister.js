require('dotenv').config();
const { APP_HOST, APP_PORT } = process.env;
// const os = require("os");
// const ip = os.networkInterfaces().en0[1].address;
// const host = `http://${ip}:${APP_PORT}`

const { seq } = require("../../configs/sequelize")
const { Client, ClientDetail, ClientDocument, Account } = require('../objects')



module.exports = async (clientData, detailData, documentData) => {
  try {
    const { id_client } = clientData
    const result = await seq.transaction(async (trans) => {
      const userClient = await Client.create({ ...clientData, status: 'inactive' }, { transaction: trans })
      const userDetail = await ClientDetail.create({ ...detailData, id_client }, { transaction: trans })
      const userAccount = await Account.create(
        {
          id_client,
          id_account: "188" + id_client
        }, { transaction: trans })

      const userDocument = await ClientDocument.create({ ...documentData, id_client }, { transaction: trans })

      const payload = {
        id: id_client,
        account_id: userAccount.id_account,
        email: userClient.email,
        phone_number: userClient.phone_number,
        name: userClient.full_name,
        ktp_number: userDetail.ktp_number,
        ktp: APP_HOST + "/clients/ktp/" + userDocument.ktp,
        picture: APP_HOST + "/clients/picture/" + userDocument.picture,
        signature: APP_HOST + "/clients/signature/" + userDocument.signature
      }
      return payload
    })

    if (result instanceof Error) throw new Error(result)
    else return result
  }
  catch (err) {
    throw new Error(err.message)
  }

}

// const db = require("../../configs/database");
// require('dotenv').config();
// const { APP_PORT } = process.env;
// const os = require("os");
// const ip = os.networkInterfaces().en0[1].address;
// const host = `http://${ip}:${APP_PORT}/`
// const { rollback } = require('../../helpers/database')

// const insertQuery = `INSERT INTO ; SET ?`

// const account = {
//   balance: 0,
//   simpanan_wajib: 0,
//   bulanan_simpanan_wajib: 0
// }

// const nowDate = new Date().getFullYear() + '-' +
//   new Date().getMonth() + '-' +
//   new Date().getDate()

// const getZeroFill = (id) => {
//   return "0".repeat(9 - id.length) + id
// }

// const table1 = `client`
// const table2 = `client_details`
// const table3 = `client_documents`
// const table4 = `accounts`

// module.exports = async (client, details, documents) => {
//   return new Promise((resolve, reject) => {
//     db.beginTransaction(transErr => {
//       if (transErr) reject(new Error("Database Gagal"))
//       else {
//         Object.assign(client, { status: 'inactive' })
//         db.query(insertQuery.replace(';', table1), client, (clientErr, insertedClient) => {
//           if (clientErr) rollback(db, clientErr, 'Insert Client Gagal', reject)
//           else if (insertedClient.affectedRows < 1) reject(new Error("Insert Client Gagal"))
//           else {
//             Object.assign(details, { id_client: insertedClient.insertId })
//             db.query(insertQuery.replace(';', table2), details, (detailsErr, insertedDetail) => {
//               if (detailsErr) rollback(db, detailsErr, 'Insert Detail Gagal', reject)
//               else if (insertedDetail.affectedRows < 1) reject(new Error("Insert Detail Error"))
//               else {
//                 const id_account = "188" + getZeroFill(insertedClient.insertId.toString())
//                 Object.assign(account, {
//                   id_client: insertedClient.insertId,
//                   id_account
//                 })
//                 db.query(insertQuery.replace(';', table4), account, (accountErr, insertedAccount) => {
//                   if (accountErr) rollback(db, accountErr, 'Insert Account Gagal', reject)
//                   else if (insertedAccount.affectedRows < 1) reject(new Error("Insert Account Error"))
//                   else {
//                     Object.assign(documents, {
//                       id_client: insertedClient.insertId,
//                     })
//                     db.query(insertQuery.replace(';', table3), documents, (documentErr, insertedDocument) => {
//                       if (documentErr) rollback(db, documentErr, 'Insert Document Gagal', reject)
//                       else if (insertedDocument.affectedRows < 1) reject(new Error("Insert Account Error"))
//                       else {
//                         db.commit((commitError) => {
//                           if (commitError) rollback(db, documentErr, 'Registrasi Anggota Gagal', reject)
//                           else {
//                             resolve({
//                               id: getZeroFill(insertedClient.insertId.toString()),
//                               account_id: id_account,
//                               email: client.email,
//                               phone_number: client.phone_number,
//                               name: client.full_name,
//                               ktp: host + "clients/ktp/" + documents.ktp,
//                               picture: host + "clients/picture/" + documents.picture,
//                               signature: host + "clients/signature/" + documents.signature
//                             })
//                           }
//                         })
//                       }
//                     })
//                   }
//                 })
//               }
//             })
//           }
//         })
//       }
//     })
//   })
// }