const { Application, DepositProduct, LoanProduct } = require('../objects')

module.exports = async (data = {}) => Application.findOne({ ...data })
  .then(value => {
    if (id_product.includes("SMPN")) {
      value.product = await DepositProduct.findOne({ where: { id_deposit_prod: id_product } })
    }
    else if (id_product.includes("PNJM")) {
      value.product = await LoanProduct.findOne({ where: { id_loan_prod: id_product } })
    }
    else throw new Error('Produk Tidak Ditemukan')
    delete value.id_product
    return { application: value }
  })
  .catch(err => {
    console.log(err)
    throw new Error(err.message)
  })