const { Application } = require('../objects')

module.exports = async (data, where) => await Application.update({ ...data }, { ...where }).then(value => value).catch(e => { throw new Error(e.message) })