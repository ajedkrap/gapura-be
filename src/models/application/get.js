const { Application } = require('../objects')

module.exports = async (data = {}) => Application.findAll({ ...data })
  .then(value => { return { application: value } })
  .catch(err => {
    console.log(err)
    throw new Error(err.message)
  })