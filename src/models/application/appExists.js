const { seq } = require("../../configs/sequelize")
const { Application, Collateral } = require('../objects')


module.exports = async (data) => {
  try {
    const result = await seq.transaction(async (trans) => {
      const getApp = await Application.findOne({ where: { ...data }, transaction: trans })
        .then(value => value.dataValues)
        .catch(err => { throw new Error(err.message) })
      if (!getApp) return [false, getApp]
      else {
        if (getApp['id_collateral'] !== null) {
          const { id_collateral } = getApplication
          const getCol = await Collateral.findOne({ where: { id_collateral }, transaction: trans })
            .then(value => value.dataValues)
            .catch(err => { throw new Error(err.message) })
          getApp.collateral = getCol
        }
        return [true, getApp]
      }
    })
    return result
  }
  catch (err) {
    throw new Error(err.Message)
  }
}