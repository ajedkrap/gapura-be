
const { seq } = require("../../configs/sequelize")
const { Loan, LoanRecord, Deposit, DepositRecord, Application } = require("../objects")

module.exports = async (data) => {
  try {
    const result = await seq.transaction(async trans => {
      const { type, appData, transData, recordsData } = data
      let passed = {}
      if (type === "loan") {
        await Loan.create({ ...transData }, { transaction: trans })
        await LoanRecord.bulkCreate(recordsData, { transaction: trans })
        passed = { ...passed, loan: transData }

      }
      else if (type === "depo") {
        await Deposit.create({ ...transData }, { transaction: trans })
        await DepositRecord.bulkCreate(recordsData, { transaction: trans })
        passed = { ...passed, deposit: transData }

      }
      else throw new Error("Tipe Produk Tidak Valid")

      const [updateData, id] = appData
      await Application.update({ ...updateData }, { where: { ...id }, transaction: trans })

      const getApp = await Application.findOne({ where: { ...id }, transaction: trans })
        .then(data => data.dataValues)
        .catch(e => { throw new Error(e.message) });

      passed = {
        ...passed,
        application: getApp,
      }

      return passed
    })
    return result
  }
  catch (e) {
    throw new Error(e.message)
  }
}