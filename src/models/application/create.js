const { Application } = require("../objects")


module.exports = async (data) => Application.create({ ...data })