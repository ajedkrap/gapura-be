const { Loan } = require("../objects")

module.exports = async (data) => await
  Loan.findOne({ where: { ...data } })
    .then(value => {
      if (value !== null) {
        return [true, value.dataValues]
      } else return [false, null]
    })
    .catch(e => { throw new Error(e.message) })