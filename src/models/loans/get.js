const { Loan } = require("../objects")

module.exports = async (data) => await
  Loan.findAll({ where: { ...data } })
    .then(value => value.dataValues)
    .catch(e => { throw new Error(e.message) })