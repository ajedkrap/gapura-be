const { seq } = require("../../configs/sequelize");
const accExists = require("./accountExists")
const { Account, Mutation, Penalty } = require("../objects");

const wajibFee = 5000

module.exports = async (data) => {

  try {
    const { receiver, nominal, tenor, transaction, new_wajib_month, new_balance, wajib_after, penalty = null } = data
    const { id_client } = receiver
    let noteWajib = 'Pembayaran Simpanan Wajib No. & Sebesar $ untuk # bulan';
    let notePenalti = 'Penalti Keterlambatan Simpanan Wajib No. & Sebesar $';
    let penaltyMutation = null
    let penaltyLog = null
    let mutations = {
      entry: 'credit',
      transaction,
    }
    let totalFee = (parseFloat(tenor) * wajibFee)

    const result = await seq.transaction(async (trans) => {
      const [_, receiverAcc] = await accExists({ id_client }, { transaction: trans })
      const { balance, id_account, wajib, wajib_month } = receiverAcc

      let newBalance = parseFloat(balance) - parseFloat(totalFee)
      let penaltyBalance = 0
      const newWajib = wajib + parseFloat(totalFee)
      const newWajibMonth = wajib_month + parseFloat(tenor)

      if (penalty !== null) {
        const { total } = penalty
        notePenalti = notePenalti.replace("&", id_client).replace("$", total)
        penaltyBalance = newBalance - total

        penaltyMutation = {
          ...mutations,
          id_account,
          note: notePenalti,
          amount: total,
          new_balance: penaltyBalance
        }

        penaltyLog = {
          id_client,
          id_account,
          reason: notePenalti,
          amount: total,
          log: JSON.stringify(penalty)
        }
      }

      if (((penaltyBalance || newBalance) !== new_balance) && (wajib_after !== newWajib) && (newWajibMonth !== new_wajib_month)) {
        throw new Error('Inquiry Invalid')
      }
      else {

        await Account.update({ balance: penaltyBalance || newBalance, wajib: newWajib, wajib_month: newWajibMonth }, { where: { id_account }, transaction: trans })
        noteWajib = noteWajib.replace("&", id_client).replace("$", `Rp.` + newWajib).replace('#', tenor.toString())

        /* SIMPANAN WAJIB + PENALTY MUTATION */
        let theMutations = [{
          ...mutations,
          id_account,
          amount: totalFee,
          new_balance: newBalance,
          note: noteWajib
        }]

        if (penaltyMutation !== null) {
          theMutations.push(penaltyMutation)
        }

        await Mutation.bulkCreate(theMutations, { transaction: trans })

        if (penaltyLog !== null) {
          await Penalty.create({ ...penaltyLog }, { transaction: trans })
        }

        const [_, newReceiverAcc] = await accExists({ id_client }, { transaction: trans })

        return {
          receiver: { ...newReceiverAcc, penalty: penalty || null },
          note: [noteWajib, penalty !== null ? notePenalti : ""].join(';')
        }
      }
    })
    return result
  }
  catch (err) {
    throw new Error(err.message)
  }
}
