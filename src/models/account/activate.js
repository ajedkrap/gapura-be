const { seq } = require("../../configs/sequelize");
const accExists = require("./accountExists")
const { Account, Mutation, Client } = require("../objects");

const wajibFee = 5000
const pokokFee = 20000

module.exports = async (data) => {

  try {
    const { receiver, nominal, tenor, transaction, new_balance, wajib_after, pokok_after } = data
    const { id_client } = receiver
    let noteWajib = 'Pembayaran Simpanan Wajib No. & Sebesar $ untuk # bulan';
    let notePokok = 'Pembayaran Simpanan Pokok No. & Sebesar $';
    let mutations = {
      entry: 'credit',
      transaction,
    }
    let activateFee = (parseFloat(tenor) * wajibFee) + (pokokFee)
    const result = await seq.transaction(async (trans) => {
      const [_, receiverAcc] = await accExists({ id_client }, { transaction: trans })
      const { balance, id_account } = receiverAcc
      const newBalance = parseFloat(balance) - parseFloat(activateFee)
      const newWajib = parseFloat(tenor) * wajibFee
      if ((newBalance !== new_balance) && (wajib_after !== newWajib) && (parseFloat(pokok_after) !== pokokFee)) {
        throw new Error('Inquiry Invalid')
      }
      else {
        await Client.update({ status: 'active' }, { where: { id_client }, transaction: trans })
        await Account.update({ balance: newBalance, wajib: newWajib, pokok: pokokFee, wajib_month: tenor }, { where: { id_account }, transaction: trans })
        noteWajib = noteWajib.replace("&", id_client).replace("$", `Rp.` + newWajib).replace('#', tenor.toString())
        notePokok = notePokok.replace("&", id_client).replace("$", `Rp.` + pokokFee)
        /* SIMPANAN WAJIB & POKOK MUTATION */
        await Mutation.bulkCreate([{
          ...mutations,
          id_account,
          amount: nominal,
          new_balance: newBalance,
          note: noteWajib
        }, {
          ...mutations,
          id_account,
          amount: nominal,
          new_balance: newBalance,
          note: notePokok
        }], { transaction: trans })

        const [_, newReceiverAcc] = await accExists({ id_client }, { transaction: trans })

        return {
          receiver: { ...newReceiverAcc, status: 'active' },
          note: [noteWajib, notePokok].join(';')
        }
      }
    })
    return result
  }
  catch (err) {
    throw new Error(err.message)
  }
}










// const db = require("../../configs/database")
// const { rollback } = require("../../helpers/database")

// const transaction = 'topup'
// const entry = 'debit'
// let note = 'Top Up Simpananku No.'

// const topUpQuery = `UPDATE accounts SET ? WHERE ?`
// const mutationQuery = `INSERT INTO mutations SET ? `
// const accountQuery = 'SELECT * FROM accounts WHERE ?'

// const topUp = (balance, nominal) => parseInt(balance) + parseInt(nominal)

// module.exports = async (data) => {
//   const { account, nominal } = data
//   const { id_account, balance } = account;
//   const newBalance = topUp(balance, nominal);
//   note += id_account.toString();
//   note += ` Sebesar Rp.${nominal},00`

//   const dataToAccount = {
//     balance: newBalance
//   }

//   const dataToMutation = {
//     id_account,
//     entry,
//     amount: nominal,
//     new_balance: newBalance,
//     transaction,
//     note
//   }

//   return new Promise((resolve, reject) => {
//     db.beginTransaction(transErr => {
//       if (transErr) rollback(db, transErr, 'Database Gagal', reject)
//       else {
//         db.query(topUpQuery, [{ ...dataToAccount }, { id_account }], (accountErr, accountRes) => {
//           if (accountErr) rollback(db, accountErr, 'Update Account Gagal', reject)
//           else if (accountRes.changedRows < 1) rollback(db, 'Affected Rows Gagal', 'Update Account Gagal', reject)
//           else {
//             db.query(mutationQuery, dataToMutation, (mutationErr, mutationRes) => {
//               if (mutationErr) rollback(db, mutationErr, 'Insert Mutation Gagal', reject)
//               else if (mutationRes.affectedRows < 1) rollback(db, 'Affected Rows Gagal', 'Insert Mutation Gagal', reject)
//               else {
//                 db.query(accountQuery, { id_account }, (accountErr, accountRes) => {
//                   if (accountErr) rollback(db, accountErr, 'Get Akun Gagal', reject)
//                   else {
//                     db.commit(commitErr => {
//                       if (commitErr) rollback(db, commitErr, 'Top Up Gagal', reject)
//                       else if (accountRes.length > 0) resolve(accountRes[0])
//                       else if (accountRes.length > 1) resolve(accountRes)
//                       else resolve(false)
//                     })
//                   }
//                 })

//               }
//             })
//           }
//         })
//       }
//     })
//   })
// }