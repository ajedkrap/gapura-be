const { Account, Client } = require('../objects')

module.exports = async (data, trans = {}) =>
  Account.findOne({ where: { ...data }, ...trans })
    .then(async (value) => {
      if (value === null) {
        return [false, null]
      }
      else {
        const userAccount = value.dataValues
        const { id_client } = userAccount
        const getUser = await Client.findOne({ where: { id_client }, ...trans }).then(value => value.dataValues).catch(err => { throw new Error(err.message) })
        console.log(getUser)
        return [true, { ...userAccount, full_name: getUser['full_name'], entry_date: getUser['entry_date'], status: getUser['status'] }]
      }
    })
    .catch(err => {
      throw new Error(err.message)
    })



// const { commit } = require("../../configs/database")
// const db = require("../../configs/database")
// const { rollback } = require("../../helpers/database")

// const tableName = 'accounts'

// module.exports = async (data) => {
//   const sql = `SELECT * FROM ${tableName} WHERE ?`

//   return new Promise((resolve, reject) => {
//     db.query(sql, data, (accountErr, accountRes) => {
//       if (accountErr) reject(new Error('Get Akun Gagal'))
//       else if (accountRes.length > 0) resolve(accountRes[0])
//       else resolve(false)
//     })
    // db.beginTransaction(transErr => {
    //   if (transErr) rollback(db, transErr, "Database Gagal", reject)
    //   else {
    //     db.query(sql, data, (accountErr, accountRes) => {
    //       if (accountErr) rollback(db, accountErr, 'Get Akun Gagal', reject)
    //       else if (accountRes.length < 1) reject(new Error('Akun Tidak Ada'))
    //       else {
    //         db.commit(commitErr => {
    //           if (commitErr) rollback(db, commitErr, 'Cek Akun Gagal', reject)
    //           else resolve(accountRes[0])
    //         })
    //       }
    //     })
    //   }
    // })
//   })
// }