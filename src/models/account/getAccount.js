const { Account } = require("../objects")

module.exports = async (data) => {
  const getAccount = await Account.findOne({ where: { ...data } })
  if (getAccount === null) return [false, null]
  else {
    let theAccount = getAccount.dataValues
    return [true, theAccount]
  }
}

// const db = require("../../configs/database")
// // const {rollback} = require("../../helpers/database")

// const sql = 'SELECT * FROM accounts WHERE ?'

// module.exports = async (data) =>
//   new Promise((resolve, reject) => {
//     db.query(sql, data, (err, res) => {
//       if (err) reject(new Error('Get Akun Gagal'))
//       else if (res.length > 0) resolve(res[0])
//       else if (res.length > 1) resolve(res)
//       else reject(new Error('Get Akun Gagal'))
//     })
//   })