const { seq } = require("../../configs/sequelize");
const accExists = require("./accountExists")
const { Account, Mutation } = require("../objects");

module.exports = async (data) => {

  try {
    const { receiver, sender, nominal, transaction, new_balance, message = '' } = data
    const { id_client: recId } = receiver
    const { id_client: senId } = sender
    let noteSender = 'Gapura Transfer ke @ No. & Sebesar $';
    let senderMutations = {
      entry: 'credit',
      transaction,
    }
    let noteReceiver = 'Gapura Transfer dari @ No. & Sebesar $';
    let receiverMutations = {
      entry: 'debit',
      transaction,
    }
    const result = await seq.transaction(async (trans) => {
      const [_sen, senderAcc] = await accExists({ id_client: senId }, { transaction: trans })
      const [_rec, receiverAcc] = await accExists({ id_client: recId }, { transaction: trans })
      const { balance: senBal, id_account: senAcc, full_name: senName } = senderAcc
      const { balance: recBal, id_account: recAcc, full_name: recName } = receiverAcc

      const senBalance = parseFloat(senBal) - parseFloat(nominal)
      const recBalance = parseFloat(recBal) + parseFloat(nominal)

      if (senBalance !== new_balance) {
        throw new Error('Inquiry Invalid')
      }
      else {
        /* GAPURA TRANSFER SENDER*/
        await Account.update({ balance: senBalance }, { where: { id_account: senAcc }, transaction: trans })

        /* GAPURA TRANSFER RECEIVER*/
        await Account.update({ balance: recBalance }, { where: { id_account: recAcc }, transaction: trans })

        /* TRANSFER MUTATION SENDER, then RECEIVER */

        noteSender = noteSender.replace("@", recName).replace("&", recId).replace("$", `Rp.` + nominal)
        noteReceiver = noteReceiver.replace("@", senName).replace("&", senId).replace("$", `Rp.` + nominal)

        await Mutation.bulkCreate([{
          ...senderMutations,
          id_account: senAcc,
          amount: nominal,
          new_balance: senBalance,
          note: noteSender
        }, {
          ...receiverMutations,
          id_account: recAcc,
          amount: nominal,
          new_balance: recBalance,
          note: noteReceiver
        }], { transaction: trans })

        const [_, newSenderAcc] = await accExists({ id_client: senId }, { transaction: trans })

        return {
          receiver,
          sender: newSenderAcc,
          note: noteSender
        }
      }
    })
    return result
  }
  catch (err) {
    throw new Error(err.message)
  }
}










// const db = require("../../configs/database")
// const { rollback } = require("../../helpers/database")

// const transaction = 'topup'
// const entry = 'debit'
// let note = 'Top Up Simpananku No.'

// const topUpQuery = `UPDATE accounts SET ? WHERE ?`
// const mutationQuery = `INSERT INTO mutations SET ? `
// const accountQuery = 'SELECT * FROM accounts WHERE ?'

// const topUp = (balance, nominal) => parseInt(balance) + parseInt(nominal)

// module.exports = async (data) => {
//   const { account, nominal } = data
//   const { id_account, balance } = account;
//   const newBalance = topUp(balance, nominal);
//   note += id_account.toString();
//   note += ` Sebesar Rp.${nominal},00`

//   const dataToAccount = {
//     balance: newBalance
//   }

//   const dataToMutation = {
//     id_account,
//     entry,
//     amount: nominal,
//     new_balance: newBalance,
//     transaction,
//     note
//   }

//   return new Promise((resolve, reject) => {
//     db.beginTransaction(transErr => {
//       if (transErr) rollback(db, transErr, 'Database Gagal', reject)
//       else {
//         db.query(topUpQuery, [{ ...dataToAccount }, { id_account }], (accountErr, accountRes) => {
//           if (accountErr) rollback(db, accountErr, 'Update Account Gagal', reject)
//           else if (accountRes.changedRows < 1) rollback(db, 'Affected Rows Gagal', 'Update Account Gagal', reject)
//           else {
//             db.query(mutationQuery, dataToMutation, (mutationErr, mutationRes) => {
//               if (mutationErr) rollback(db, mutationErr, 'Insert Mutation Gagal', reject)
//               else if (mutationRes.affectedRows < 1) rollback(db, 'Affected Rows Gagal', 'Insert Mutation Gagal', reject)
//               else {
//                 db.query(accountQuery, { id_account }, (accountErr, accountRes) => {
//                   if (accountErr) rollback(db, accountErr, 'Get Akun Gagal', reject)
//                   else {
//                     db.commit(commitErr => {
//                       if (commitErr) rollback(db, commitErr, 'Top Up Gagal', reject)
//                       else if (accountRes.length > 0) resolve(accountRes[0])
//                       else if (accountRes.length > 1) resolve(accountRes)
//                       else resolve(false)
//                     })
//                   }
//                 })

//               }
//             })
//           }
//         })
//       }
//     })
//   })
// }