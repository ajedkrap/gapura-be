const { Officer } = require("../objects")

module.exports = async (data, _) => Officer.count({ ...data })