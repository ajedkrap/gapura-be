require('dotenv').config()
const { DB_NAME_DEV } = process.env
const { seq } = require('../../configs/sequelize')
const { Officer } = require("../objects")



const sqlincrement = `SELECT SUM(\`auto_increment\`) as increment FROM INFORMATION_SCHEMA.TABLES WHERE table_name='officers' AND table_schema='${DB_NAME_DEV}'`


module.exports = async (data) => {
  try {
    const result = await seq.transaction(async (trans) => {
      const [results, _] = await seq.query(sqlincrement, { transaction: trans })
      const increment = results[0].increment
      const officerExist = await Officer.findAll({ where: { ...data } }, { transaction: trans })
      if (officerExist.length > 0) return [true, officerExist[0].dataValues]
      else return [false, increment]
    })
    return result
  }
  catch (err) {
    throw new Error(err.message)
  }
}



// require('dotenv').config()
// const db = require("../../configs/database")
// const { rollback } = require("../../helpers/database")
// const { DB_NAME_DEV } = process.env



// const incrementData = [
//   { table_name: 'officers' },
//   { table_schema: DB_NAME_DEV },
// ]

// const sql1 = `SELECT * FROM officers WHERE ?`
// const sqlincrement = `SELECT SUM(\`auto_increment\`) as increment FROM INFORMATION_SCHEMA.TABLES WHERE ? AND ?`

// module.exports = (data) => {
//   return new Promise((resolve, reject) => {
//     db.beginTransaction(transErr => {
//       if (transErr) rollback(db, transErr, "Database Gagal", reject)
//       else {
//         db.query(sqlincrement, incrementData, (incrementErr, incrementRes) => {
//           if (incrementErr) rollback(db, incrementErr, "Get Increment Gagal", reject);
//           else {
//             db.query(sql1, data, (emailExistErr, emailExistRes) => {
//               if (emailExistErr) rollback(db, emailExistErr, "Get Email Gagal", reject);
//               else {
//                 db.commit(commitError => {
//                   if (commitError) rollback(db, commitError, "Cek Email Gagal", reject);
//                   else {
//                     if (emailExistRes.length > 1) resolve({ status: true, data: emailExistRes[0] })
//                     else if (emailExistRes.length > 0) resolve({ status: true, data: emailExistRes[0] })
//                     else resolve({
//                       status: false,
//                       increment: incrementRes[0].increment
//                     })
//                   }
//                 })
//               }
//             })
//           }
//         })
//       }
//     })
//   })
// }