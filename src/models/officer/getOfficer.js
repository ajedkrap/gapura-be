const { Officer } = require("../objects")

module.exports = async (data, _) =>
  Officer.findAll({ ...data })
    .then(value => { return { officer: value } })
    .catch(err => {
      console.log(err)
      throw new Error(err.message)
    })
