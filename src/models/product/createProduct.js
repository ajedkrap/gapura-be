const { DepositProduct, LoanProduct } = require("../objects")

module.exports = async (data, type) => type === "depo" ? DepositProduct.create({ ...data }) : LoanProduct.create({ ...data })