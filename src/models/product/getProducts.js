
const { seq } = require("../../configs/sequelize")

const { DepositProduct, LoanProduct } = require("../objects")


module.exports = async (query = {}, type) => {
  const id = query.id || ""
  try {
    let product = {}
    const result = await seq.transaction(async (trans) => {
      if (id && id !== "") {
        const depositProd = await DepositProduct.findAll({ where: { id_deposit_prod: id }, transaction: trans })
        const loanProd = await LoanProduct.findAll({ where: { id_loan_prod: id }, transaction: trans })
        if (depositProd.length > 0) product = { ...product, ...depositProd }
        if (loanProd.length > 0) product = { ...product, ...loanProd }
      }
      else if (type && type !== "") {
        if (type === "depo") {
          const depositProd = await DepositProduct.findAll({ ...query, transaction: trans })
          if (depositProd.length > 0) product = { ...product, depositProd }
        }
        else if (type === "loan") {
          const loanProd = await LoanProduct.findAll({ ...query, transaction: trans })
          if (loanProd.length > 0) product = { ...product, loanProd }
        }
      }
      else {
        const depositProd = await DepositProduct.findAll({ transaction: trans })
        const loanProd = await LoanProduct.findAll({ transaction: trans })
        if (depositProd.length > 0) product = { ...product, depositProd }
        if (loanProd.length > 0) product = { ...product, loanProd }
      }
      return product
    })

    return result
  }
  catch (err) {
    throw new Error(err.message)
  }
}

// const db = require("../../configs/database");
// const { rollback } = require("../../helpers/database")


// let sql = "SELECT * FROM & "

// let t1 = "deposit_type"
// let t2 = "loan_type"

// module.exports = async () =>
//   new Promise((resolve, reject) => {
//     db.beginTransaction(transErr => {
//       if (transErr) reject(new Error("Database Gagal"))
//       else {
//         db.query(sql.replace("&", t1), async (depTypErr, depTypRes) => {
//           if (depTypErr) await rollback(db, depTypErr, "Get Deposit Type Gagal", reject)
//           else {
//             db.query(sql.replace("&", t2), async (loanTypErr, loanTypRes) => {
//               if (loanTypErr) await rollback(db, loanTypErr, "Get Deposit Type Gagal", reject)
//               else {
//                 db.commit(async commitErr => {
//                   if (commitErr) await rollback(db, commitErr, "Get Products Gagal", reject)
//                   else resolve({
//                     depositTypes: depTypRes,
//                     loanTypes: loanTypRes
//                   })
//                 })
//               }
//             })
//           }
//         })
//       }
//     })
//   })
