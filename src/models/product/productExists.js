require('dotenv').config()
const { DB_NAME_DEV } = process.env
const { seq } = require("../../configs/sequelize")
const { DepositProduct, LoanProduct } = require("../objects")

const sqlincrement = (type) => `SELECT SUM(\`auto_increment\`) as increment FROM INFORMATION_SCHEMA.TABLES WHERE table_name='${type === "depo" ? "deposit_products" : "loan_products"}' AND table_schema='${DB_NAME_DEV}'`


module.exports = async (data, type) => {
  if (type === "depo") {
    try {
      const result = await seq.transaction(async (depoTrans) => {
        const [results, _] = await seq.query(sqlincrement(type), { transaction: depoTrans })
        const increment = results[0].increment
        const productExist = await DepositProduct.findAll({ where: { ...data }, transaction: depoTrans })
        if (productExist.length > 0) return [true, productExist[0].dataValues]
        else return [false, increment]
      })
      return result
    }
    catch (err) {
      throw new Error(err.message)
    }
  }
  else if (type === "loan") {
    try {
      const result = await seq.transaction(async (loanTrans) => {
        const [results, _] = await seq.query(sqlincrement(type), { transaction: loanTrans })
        const increment = results[0].increment
        const productExist = await LoanProduct.findAll({ where: { ...data }, transaction: loanTrans })
        if (productExist.length > 0) return [true, productExist[0].dataValues]
        else return [false, increment]
      })
      return result
    }
    catch (err) {
      throw new Error(err.message)
    }
  }
  else {
    throw new Error("Cek Product Gagal")
  }
}
