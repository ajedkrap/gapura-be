const { seq } = require("../../configs/sequelize")
const { Application, Deposit, Loan, Client } = require("../objects")

module.exports = async () => {
  try {
    const result = await seq.transaction(async (trans) => {
      const clients = await Client.findAll({ transaction: trans })
      const applyData = await Application.findAll({ transaction: trans })
      const deposits = await Deposit.findAll({ transaction: trans })
      const loans = await Loan.findAll({ transaction: trans })

      return { clients, applyData, deposits, loans }
    })

    return result
  } catch (e) {
    throw new Error(e.message)
  }
}