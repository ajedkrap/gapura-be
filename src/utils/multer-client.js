const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, `./src/assets/clients/${file.fieldname}`);
  },
  filename: function (req, file, callback) {
    const date = Date.now() + path.extname(file.originalname)
    switch (file.fieldname) {
      case "ktp":
        callback(null, "KTP_" + date);
        break;
      case "picture":
        callback(null, "PIC_" + date);
        break;
      case "signature":
        callback(null, "SIGN_" + date);
        break;
    }
  }
})
const upload = multer({
  storage,
  limits: { fileSize: 1024 * 1024 },
  fileFilter: function (req, file, callback) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/i)) {
      return callback(new Error("Only image files are allowed!"), false);
    }
    callback(null, true);
  }
})

module.exports = upload.fields([
  { name: "ktp", maxCount: 1 },
  { name: "picture", maxCount: 1 },
  { name: "signature", maxCount: 1 }
]);