const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, `./src/assets/collateral`);
  },
  filename: function (req, file, callback) {
    const clientId = '_' + req.body['id_client']
    const date = Date.now()
    callback(null, "AGUNAN_" + date + clientId + path.extname(file.originalname));
  }
})
const upload = multer({
  storage,
  limits: { fileSize: 1024 * 1024 },
  fileFilter: function (req, file, callback) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/i)) {
      return callback(new Error("Only image files are allowed!"), false);
    }
    callback(null, true);
  }
})

module.exports = upload.single('colPhoto');