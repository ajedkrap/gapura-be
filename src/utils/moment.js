const moment = require('moment');
moment.locale('id')

module.exports = {
  date: (date) => moment(date).format("YYYY-MM-DD"),
  getMonth: (date) => moment(date).format('MMMM'),
  milliseconds: (date) => moment(date).format("x"),
  endOfMonth: (date) => moment(date).endOf("M").format("x"),
  wajibDueDate: (entry_date, month) => moment(entry_date, 'YYYY-MM-DD').add(month, 'M'),
  getCurrentDate: () => moment().set({ 'hour': 0, 'minute': 0, 'second': 0 })
}