const { seq } = require("../configs/sequelize")
const { Account, Mutation } = require("../models/objects")
const getClientData = require("../models/client/getClient")
const moment = require('moment')

const wajibFee = 5000

const autoPay = async (autoPayStart = false) => {
  if (autoPayStart) {
    const clients = await getClientData().then(value => value['clients'].map(val => { return { client: val.client, account: val.accounts } })
    ).catch(err => { throw new Error(err.message) })
    try {
      await seq.transaction(async trans => {
        await clients.forEach(async (value) => {
          const { client, account } = value
          if (client.status === "active") {
            const { entry_date, id_client, full_name } = client
            const { id_account, balance, wajib, wajib_month } = account
            const totalFee = wajibFee
            if (moment(entry_date).add(wajib_month, "M").valueOf() < moment().valueOf()) {
              if (balance > totalFee) {
                let balanceBefore = parseFloat(balance)
                let wajibBefore = parseFloat(wajib)
                let wajibMonthBefore = parseFloat(wajib_month)
                let newBalance = balanceBefore - totalFee
                let newWajib = wajibBefore + totalFee
                let newWajibMonth = wajibMonthBefore + 1
                noteWajib = noteWajib.replace("&", id_client).replace("$", wajibFee).replace("#", "1")
                await Account.update({
                  balance: newBalance,
                  wajib: newWajib,
                  wajib_month: newWajibMonth
                }, { where: { id_account }, transaction: trans })
                await Mutation.create({
                  entry: 'credit',
                  transaction: 'koperasi',
                  id_account,
                  new_balance: newBalance,
                  amount: totalFee,
                  note: noteWajib
                }, { transaction: trans })
              }
              // NOTIFY OVERDUE SIMPANAN WAJIB else{   return {  id_client, id_account,full_name,message:  "Saldo Tak Cukup"}}
            }
          }
          // NOTIFY INACTIVE ACCOUNT else { return {id_client: client['id_client'],id_account: account['id_account'],full_name: client['full_name'],message:  "Please Activate before " + moment(client["entry_date"]).add(90,"d").format("dddd, DD-MM-YYYY HH:mm")} }
        })
      })
    }
    catch (err) {
      throw new Error(err.message)
    }
    console.log("Auto Simpanan Wajib")
    setTimeout(() => {
      autoPay(true)
    }, moment().endOf('M').valueOf() - moment().valueOf())
  }
  else {
    console.log("Auto Simpanan Wajib Payment Start")
    setTimeout(() => {
      autoPay(true)
    }, moment().endOf('M').valueOf() - moment().valueOf())
  }
}

module.exports = autoPay 