require("dotenv").config();
const qs = require("query-string");
const { Op } = require('sequelize')
const { APP_HOST } = process.env;



const getSort = (sort, sortBy = "date") => {
  return {
    order: [
      [sortBy === "date" ? "updated_at" : sortBy, sort === "desc" ? "DESC" : "ASC"]
    ]
  }
};

const getSearch = (model, search) => {
  let where
  if (search && search !== "") {
    switch (model) {
      case "officer":
      case "client":
        where = { full_name: { [Op.substring]: search } }
        break;
      default:
        where = { name: { [Op.substring]: search } }
        break;
    }
  }
  else where = {}

  return { where }
}

module.exports = async (
  query,
  modelData,
  modelCount,
  model,
  path
) => {
  const { page, limit, sort, sortBy, search, type = "" } = query;

  const _page = page && parseInt(page) > 0 ? parseInt(page) : 1;
  const _limit = limit && parseInt(limit) > 0 ? parseInt(limit) : 5;
  const _sort = getSort(sort, sortBy);
  const _search = getSearch(model, search)

  let totalData = await modelCount(_search, type).then(value => value).catch(err => {
    throw new Error(err.message)
  });;
  const totalPage = Math.ceil(totalData / _limit);
  const start = _page * _limit - _limit;

  const prevLink = _page > 1
    ? qs.stringify({ ...query, ...{ page: _page - 1 } })
    : null;
  const nextLink = _page < totalPage
    ? qs.stringify({ ...query, ...{ page: _page + 1 } })
    : null;

  const result = await modelData({
    ..._sort,
    ..._search,
    ...{ limit: _limit },
    ...{ offset: start }
  }, type)

  return {
    result,
    pageInfo: {
      page: _page,
      totalPage,
      perPage: _limit,
      totalData,
      nextLink: nextLink && APP_HOST + `/${path}` + `?${nextLink}`,
      prevLink: prevLink && APP_HOST + `/${path}` + `?${prevLink}`,
    },
  };
};