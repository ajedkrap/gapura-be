const response = require("../../helpers/response")
const interestCalculation = require("../../utils/interestCalculation")

module.exports = async (data) => {
  const { applicationData, productData } = data
  try {
    const interestValue = await interestCalculation(productData, applicationData)
    return ({
      status: 200,
      ...response(true, "Calculate Interest", interestValue)
    })
  }
  catch (err) {
    return ({
      status: 500,
      ...response(false, e.message)
    })
    // return ({
    //   status: 200,
    //   ...response(true, "Get Data from " + id_client, clientData)
    // })
    // res.status(500).send(response(false, err.message))
  }
}