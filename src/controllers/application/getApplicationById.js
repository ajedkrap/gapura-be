const response = require("../../helpers/response")
const getApplication = require("../../models/application/get")

module.exports = async (req, res) => {
  try {
    const getApplications = await getApplication()
    res.status(200).send(response(true, "List of Application", getApplications))
  }
  catch (err) {
    res.status(500).send(response(false, err.message))
  }
}