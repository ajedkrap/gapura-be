const response = require("../../helpers/response")
const valid = require("../../validators/application")
const createApp = require("../../models/application/create")
const createCol = require("../../models/collateral/increment")
const upload = require("../../utils/multer-collateral")
const moment = require('moment')

module.exports = async (req, res) => {
  upload(req, res, async (fileError) => {
    if (fileError instanceof multer.MulterError) {
      res.status(400).send(response(false, fileError.message))
    }
    else if (fileError) {
      res.status(400).send(response(false, fileError.message))
    }
    else {
      try {
        const { id_officer = "", full_name } = req.payload
        const { status, message, passed } = await valid.create(req.body)
        if (!status) {
          res.status(400).send(response(status, message))
        }
        else if (id_officer !== "") {
          let msgData = {
            create: {
              by: id_officer,
              name: full_name,
              datetime: moment().toISOString()
            }
          }
          const { appData, colData = {} } = passed
          if (Object.keys(colData).length > 1) {
            if (!req.file < 1) {
              res.status(400).send(response(false, "Photo Agunan Tidak Ditemukan"))
            }
            else {
              const addCol = await createCol({ ...colData, photo: req.files.filename })
            }
          }
          const applying = await createApp({ ...appData, message: JSON.stringify(msgData) })
          res.status(201).send(response(status, message, applying))
        }
        else {
          const { appData, colData = {} } = passed
          if (Object.keys(colData).length > 1) {
            if (!req.file < 1) {
              res.status(400).send(response(false, "Photo Agunan Tidak Ditemukan"))
            }
            else {
              const addCol = await createCol({ ...colData, photo: req.file.filename })
            }
          }
          const applying = await createApp({ ...appData, message: JSON.stringify(msgData) })
          res.status(201).send(response(status, msg, applying))
        }
      }
      catch (err) {
        res.status(500).send(response(false, err.message))
      }
    }
  })
}
