const response = require("../../helpers/response")
const valid = require("../../validators/application")
const updateApp = require("../../models/application/update")
const approve = require("../../models/application/approve")

module.exports = async (req, res) => {
  const { reason = "" } = req.query
  try {
    if (reason === "") {
      res.status(400).send(response(false, "Reason Query Need to be Field"))
    } else {
      const { id_officer, full_name } = req.payload
      switch (reason) {
        case "review": {
          const { status, message, passed } = await valid.progress({ ...req.params, id_officer, full_name })
          if (!status) {
            res.status(400).send(response(status, message));
          }
          else {
            const [data, id] = passed
            await updateApp({ ...data }, { where: { ...id } })
            res.status(202).send(response(status, message, { ...data }));
          }
          break;
        }
        case "survey": {
          const { status, message, passed } = await valid.follow({ ...req.params, id_officer, full_name })
          if (!status) {
            res.status(400).send(response(status, message));
          }
          else {
            const [data, id] = passed
            await updateApp({ ...data }, { where: { ...id } })
            res.status(202).send(response(status, message, { ...data }));
          }
          break;
        }
        case "approve": {
          const { status, message, passed } = await valid.approval({ ...req.params, ...req.body, id_officer, full_name })
          if (!status) {
            res.status(400).send(response(status, message));
          } else {
            const approvedApp = await approve(passed)
            res.status(201).send(response(status, message, approvedApp))
          }
          break;
        }
        case "reject": {
          const { status, message, passed } = await valid.reject({ ...req.params, ...req.body, id_officer, full_name })
          if (!status) {
            res.status(400).send(response(status, message));
          } else {
            const [data, id] = passed
            await updateApp({ ...data }, { where: { ...id } })
            res.status(202).send(response(status, message, { ...data }));
          }
          break;
        }
        default: {
          res.status(400).send(response(false, "Reason Query Gagal"))
          break;
        }
      }
    }
  }
  catch (err) {
    res.status(500).send(response(false, err.message))
  }
}