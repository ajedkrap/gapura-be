const e = require("cors")
const response = require("../../helpers/response")
const getById = require("../../models/application/get")

module.exports = async (req, res) => {
  try {
    const { id: id_application } = req.params
    const getApplication = await getById({ id_application })
    if (getApplication.length < 1) {
      res.status(400).send(response(false, `Pengajuan ${id_application} Tidak Ada`))
    } else {
      res.status(200).send(response(true, "Pengajuan Sukses", getApplication))
    }
  }
  catch (err) {
    res.status(500).send(response(false, err.message))
  }
}