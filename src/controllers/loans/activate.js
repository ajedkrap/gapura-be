const response = require("../../helpers/response")
const { activate } = require("../../validators/loan")

module.exports = {
  inquiry: async (req, res) => {
    const { id_client } = req.payload
    try {
      const { status, message, passed } = await activate.inquiry({ ...req.params, id_client })
      if (!status) res.status(400).send(response(status, message))
      else {
        res.status(201).send(response(status, message, passed))
      }
    }
    catch (err) {
      res.status(500).send(response(false, err.message))
    }
  },
  payment: async (req, res) => {
    try {
      res.status(200).send(response(true, "OK", req.body))
    }
    catch (err) {
      res.status(500).send(response(false, err.message))
    }
  }
}