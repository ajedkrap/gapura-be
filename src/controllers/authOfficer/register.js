const response = require("../../helpers/response")
const upload = require("../../utils/multer-officer")
const multer = require("multer")
const valid = require("../../validators/authOfficer")
const registering = require("../../models/auth/officerRegister")
const { deleteFile } = require("../../helpers/fs")

module.exports = (req, res) => {
  upload(req, res, async (fileError) => {
    try {
      if (fileError instanceof multer.MulterError) {
        res.status(400).send(response(false, fileError.message))
      }
      else if (fileError) {
        res.status(400).send(response(false, fileError.message))
      }
      else if (!req.file) {
        res.status(400).send(response(false, "File empty"))
      }
      else {
        const { status, message, passed } = await valid.register(req.body)
        if (!status) {
          deleteFile(req.file.path);
          res.status(400).send(response(status, message));
        } else {
          Object.assign(passed, { photo: req.file.filename })
          const registeredOfficer = await registering(passed);
          console.log(registeredOfficer)
          delete registeredOfficer.password
          res.status(201).send(response(true, message, registeredOfficer))
        }
      }
    }
    catch (err) {
      deleteFile(req.file.path);
      res.status(500).send(response(false, err.message))
    }
  })
}