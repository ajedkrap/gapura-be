const response = require("../../helpers/response")
const { topUp } = require("../../validators/account")
const toppingUp = require("../../models/account/topUp")

module.exports = {
  inquiry: async (req, res) => {
    const { id_client } = req.payload
    try {
      const { status, message, passed } = await topUp.inquiry({ id_client, ...req.body })
      if (!status) res.status(400).send(response(status, message))
      else {
        res.status(201).send(response(status, message, passed))
      }
    }
    catch (err) {
      res.status(500).send(response(false, err.message))
    }
  },
  payment: async (req, res) => {
    try {
      const { receiver, note } = await toppingUp({ ...req.payload })
      res.status(200).send(response(true, note, { receiver }))
    }
    catch (error) {
      res.status(500).send(response(false, error.message))
    }
  }
}