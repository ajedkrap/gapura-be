const response = require("../../helpers/response")
const { gapuraTransfer } = require("../../validators/account")
const gapuraTransfering = require("../../models/account/gapuraTransfer")

module.exports = {
  inquiry: async (req, res) => {
    const { id_client } = req.payload
    try {
      const { status, message, passed } = await gapuraTransfer.inquiry({ id_client, ...req.body })
      if (!status) res.status(400).send(response(status, message))
      else {
        res.status(201).send(response(status, message, passed))
      }
    }
    catch (err) {
      res.status(500).send(response(false, err.message))
    }
  },
  payment: async (req, res) => {
    try {
      const { receiver, sender, note } = await gapuraTransfering({ ...req.payload })
      res.status(202).send(response(true, note, { receiver, sender }))
      // res.status(200).send(response(true, "OK", { ...req.payload }))
    }
    catch (error) {
      res.status(500).send(response(false, error.message))
    }
  }
}