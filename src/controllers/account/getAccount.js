const response = require("../../helpers/response")
const accountDetail = require("../../models/account/getAccount")

module.exports = async (req, res) => {
  const { id: id_client, full_name } = req.payload
  try {
    const getAccount = await accountDetail({ id_client })
    res.status(200).send(response(true, `Account Detail of ${full_name}`, getAccount))
  }
  catch (e) {
    res.status(500).send(response(false, e.message))
  }
}