const response = require("../../helpers/response")

module.exports = async (req, res) => {
  try {
    res.status(200).send(response(true, "OK", req.body))
  }
  catch (err) {
    res.status(500).send(response(false, err.message))
  }
}