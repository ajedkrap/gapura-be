const response = require("../../helpers/response")
const valid = require("../../validators/account")
const wajib = require("../../models/account/simpananWajib")


module.exports = async (req, res) => {
  const { id: id_client } = req.payload
  try {
    const { status, message, passed } = await valid.wajibPokok({ id_client })
    if (!status) res.status(404).send(response(status, message))
    else {
      const wajibPokokPayment = await wajib(passed, 'wajibpokok')
      res.status(202).send(response(status, message, wajibPokokPayment))
    }
  }
  catch (err) {
    res.status(500).send(response(false, err.message))
  }
}