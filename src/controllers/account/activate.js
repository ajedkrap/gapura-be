const response = require("../../helpers/response")
const { activate } = require("../../validators/account")
const activating = require("../../models/account/activate")

module.exports = {
  inquiry: async (req, res) => {
    const { id_client } = req.payload
    try {
      const { status, message, passed } = await activate.inquiry({ id_client, ...req.body })
      if (!status) res.status(400).send(response(status, message))
      else {
        res.status(201).send(response(status, message, passed))
      }
    }
    catch (err) {
      res.status(500).send(response(false, err.message))
    }
  },
  payment: async (req, res) => {
    try {
      const { receiver, note } = await activating({ ...req.payload })
      res.status(200).send(response(true, note, { receiver }))
    }
    catch (error) {
      res.status(500).send(response(false, error.message))
    }
  }
}