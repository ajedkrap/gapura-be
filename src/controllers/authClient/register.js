const response = require("../../helpers/response")
const valid = require("../../validators/authClient")
const upload = require("../../utils/multer-client")
const multer = require("multer")
const register = require("../../models/auth/clientRegister")
const { deleteFile } = require("../../helpers/fs")

module.exports = async (req, res) => {
  upload(req, res, async (fileError) => {
    try {
      if (fileError instanceof multer.MulterError) {
        res.status(400).send(response(false, fileError.message))
      }
      else if (fileError) {
        res.status(400).send(response(false, fileError.message))
      }
      else if (!req.files || req.files.length < 1) {
        res.status(400).send(response(false, "File empty"))
      }
      else {
        const { ktp: ktpFile, picture: picFile, signature: signFile } = req.files
        if (!ktpFile || !picFile || !signFile) {
          for (let obj in req.files) {
            deleteFile(req.files[obj][0].path)
          }
          res.status(400).send(response(false, "One of the File Required"))
        }
        else {
          const { status, message, passed } = await valid.register(req.body)
          if (!status) {
            for (let obj in req.files) {
              deleteFile(req.files[obj][0].path)
            }
            res.status(400).send(response(status, message))
          } else {
            const { client, details } = passed
            const documents = {
              ktp: ktpFile[0].filename,
              picture: picFile[0].filename,
              signature: signFile[0].filename
            }
            const registeredUser = await register(client, details, documents)
            if (registeredUser instanceof Error) throw new Error(registeredUser)
            else {
              res.status(201).send(response(
                status,
                message + ", ID: " + registeredUser.id,
                registeredUser
              ))
            }
          }
        }
      }
    }
    catch (err) {
      for (let obj in req.files) {
        deleteFile(req.files[obj][0].path)
      }
      res.status(500).send(response(false, err.message))
    }

  })
}

