const response = require("../../helpers/response")
const valid = require("../../validators/authClient")

module.exports = async (req, res) => {
  try {
    const { status, message, passed } = await valid.login(req.body);
    if (!status) res.status(400).send(response(status, message));
    else res.status(202).send(response(status, message, passed));
  }
  catch (e) {
    res.status(500).send(response(false, e.message))
  }
}