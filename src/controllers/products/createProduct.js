const response = require("../../helpers/response")
const valid = require("../../validators/product")
const create = require("../../models/product/createProduct")

module.exports = async (req, res) => {
  const { type } = req.params
  const { id_officer } = req.payload
  try {
    const { status, message, passed } = await valid.create({ ...req.body, type })
    if (!status) {
      res.status(400).send(response(status, message, Object.keys(passed).length ? passed : {}))
    } else {
      const createProduct = await create({ ...passed, created_by: id_officer }, type)
      res.status(201).send(response(true, message, createProduct))
    }
  }
  catch (err) {
    res.status(500).send(response(false, err.message))
  }
}