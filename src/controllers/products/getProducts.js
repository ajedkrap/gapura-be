const response = require("../../helpers/response")
const getAllProducts = require("../../models/product/getProducts")
const productCount = require("../../models/product/productCount")
const pagination = require("../../utils/pagination")

module.exports = async (req, res) => {
  const { type = "", id } = req.query
  try {
    if (id && id !== null) {
      const gettingProducts = await getAllProducts(req.query);
      if (Object.keys(gettingProducts).length < 1) {
        res.status(400).send(response(false, ` Product Empty or Not Found`));
      }
      else {
        res.status(200).send(response(true, `Get Products by Id Success`, gettingProducts[0]));
      }
    }
    else {
      switch (type) {
        case "loan": {
          const { result, pageInfo } = await pagination(
            req.query,
            getAllProducts,
            productCount,
            "product",
            "product"
          );
          res.status(200).send(response(true, "Daftar Produk Pinjaman", result, { pageInfo }));
          break;
        }
        case "depo": {
          const { result, pageInfo } = await pagination(
            req.query,
            getAllProducts,
            productCount,
            "product",
            "product"
          );
          res.status(200).send(response(true, "Daftar Product Simpanan", result, { pageInfo }));
          break;
        }
        default: {
          const gettingProducts = await getAllProducts(req.query);
          if (Object.keys(gettingProducts).length < 1) {
            res.status(400).send(response(false, `Products Empty or Not Found`));
          }
          else {
            res.status(200).send(response(true, `List of All Products`, gettingProducts));
          }
          break;
        }

      }
    }
    // const gettingProducts = await getAllProducts(req.query);
    // if (Object.keys(gettingProducts).length < 1) {
    //   res.status(200).send(response(false, `${type === "" ? "" : type === "depo" ? "Deposit" : "Loan"} Products Empty or Not Found`));
    // }
    // else {
    //   res.status(200).send(response(true, `List of ${type === "" ? "" : type === "depo" ? "Deposit" : "Loan"} Products`, gettingProducts));
    // }
  }
  catch (err) {
    res.status(500).send(response(false, err.message));
  }
}