const response = require('../../helpers/response')
const valid = require('../../validators/otp')

module.exports = async (req, res) => {
  try {
    const { status, message } = await valid.verify(req.body)
    if (!status) {
      res.status(400).send(response(status, message))
    } else {
      res.status(202).send(response(status, message))
    }
  }
  catch (err) {
    res.status(500).send(response(false, "Verifikasi OTP Gagal, " + err.message))
  }
}