const response = require('../../helpers/response')
const valid = require("../../validators/otp")
const crypt = require("crypto-js")
const otpGenerator = require("../../utils/OTPGenerator")
require('dotenv').config()

const { APP_OTP_KEY: key } = process.env
const fiveMinutes = 5 * 60 * 1000 // 5 minutes in milliseconds

module.exports = async (req, res) => {
  try {
    const { status, message, passed } = await valid.create(req.body)
    if (!status) {
      res.status(400).send(response(status, message))
    } else {
      const { phone_number } = passed
      const expires = Date.now() + fiveMinutes
      const otp = await otpGenerator(6)
      const data = {
        phone_number,
        otp,
        expires
      }
      const fullhash = `${crypt.AES.encrypt(JSON.stringify(data), key).toString()}.${expires}`
      const dataToSent = {
        hash: fullhash,
        otp
      }
      res.status(200).send(response(status, message, dataToSent))
    }
  }
  catch (err) {
    res.status(500).send(response(false, "Kirim OTP Gagal, " + err.message))
  }
}