const response = require("../../helpers/response");
const pagination = require("../../utils/pagination");
const officerModel = require("../../models/officer/getOfficer")
const officerCountModel = require("../../models/officer/getOfficerCount")

module.exports = async (req, res) => {
  try {
    const { result, pageInfo } = await pagination(
      req.query,
      officerModel,
      officerCountModel,
      "officer",
      "officer"
    );
    res.status(200).send(response(true, "Daftar Officers", result, { pageInfo }));
  } catch (e) {
    res
      .status(500)
      .send(
        response(false, "Internal Server Error: " + e.message)
      );
  }
}