const response = require('../../helpers/response')
const listAreas = require("../../models/area/listArea")

const getLevel = async (code = "") => {
  switch (code.length) {
    case 0:
      return "Provinsi"
    case 2:
      return "Kota/Kabupaten"
    case 5:
      return "Kecamatan"
    case 8:
      return "Kelurahan"
  }
}

module.exports = async (req, res) => {
  const { area_code: areaCode = "" } = req.query
  if (areaCode && areaCode.length > 8) {
    res.status(404).send(response(false, "Daftar Area Tidak Ditemukan"))
  }
  else {
    try {
      const level = await getLevel(areaCode)
      const listArea = await listAreas(areaCode)
      res.status(200).send(response(
        true,
        "Daftar " + level,
        listArea
      ))
    }
    catch (err) {
      res.status(500).send(response(false, "Error: " + err.message))
    }
  }
}