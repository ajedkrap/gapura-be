const response = require('../../helpers/response')
const getAreas = require("../../models/area/getArea")


module.exports = async (req, res) => {
  const { area_code: areaCode } = req.query
  if (areaCode.length !== 13) {
    res.status(400).send(response(false, "Kode Salah"))
  }
  else {
    try {
      const getArea = await getAreas(areaCode)
      if (typeof getArea === 'object') {
        res.status(200).send(response(true, "Alamat Sukses", getArea))
      } else {
        res.status(400).send(response(false, "Kode Invalid"))
      }
    }
    catch (err) {
      res.status(500).send(response(false, "Error: " + err.message))
    }
  }
}