const response = require("../../helpers/response");
const pagination = require("../../utils/pagination");
const clientModel = require("../../models/client/getClient")
const clientCountModel = require("../../models/client/getClientCount")

module.exports = async (req, res) => {
  try {
    const { result, pageInfo } = await pagination(
      req.query,
      clientModel,
      clientCountModel,
      "client",
      "client/profile"
    );
    res.status(200).send(response(true, "Daftar Anggota", result, { pageInfo }));
  } catch (e) {
    res
      .status(500)
      .send(
        response(false, "Failed to get list clients [Internal error server]")
      );
  }
}