const response = require("../../helpers/response")
const getClientData = require("../../models/client/getClientData")

module.exports = async (id_client = userId) => {
  try {
    const clientData = await getClientData({ id_client });
    return ({
      status: 200,
      ...response(true, "Get Data from " + id_client, clientData)
    })
    // res.status(200).send(response(true, "Get Data from " + id_client, clientData))
  } catch (e) {
    return ({
      status: 500,
      ...response(false, e.message)
    })

    // res.status(500).send(response(false, e.message))
  }
}