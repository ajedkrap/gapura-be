const response = require("../../helpers/response")
const getCoopData = require("../../models/coop/getCoopData")

module.exports = async () => {
  try {
    const coopData = await getCoopData()
    return ({
      status: 200,
      ...response(true, "Data Koperasi Berhasil", coopData)
    })
    // res.status(200).send(response(true, "OK", req.body))
  }
  catch (err) {
    return ({
      status: 500,
      ...response(false, "Gagal Mengambil Data Koperasi")
    })
    // res.status(500).send(response(false, err.message))
  }
}