module.exports = {
  rollback: async (db, error, message, reject) =>
    db.rollback((rollbackError) => {
      console.log(error)
      if (rollbackError) reject(new rollbackError.message)
      else reject(new Error(message));
    })

}