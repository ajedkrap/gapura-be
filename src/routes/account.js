const route = require("express").Router();
const getAccount = require("../controllers/account/getAccount")
const topUp = require("../controllers/account/topup")
const activate = require("../controllers/account/activate")
const gapuraTransfer = require("../controllers/account/gapuraTransfer")
// const wajibPokok = require("../controllers/account/wajibPokok")
const { verifyPayment, allowUser, allowOperational } = require('../middlewares/verifyJwt')

route
  // .get("/", allowUser, getAccount)
  .get("/topup/inquiry", allowUser, topUp.inquiry)
  .patch("/topup/payment", verifyPayment, topUp.payment)
  .get("/activate/inquiry", allowUser, activate.inquiry)
  .patch("/activate/payment", verifyPayment, activate.payment)
  .get("/gapura/inquiry", allowUser, gapuraTransfer.inquiry)
  .patch("/gapura/payment", verifyPayment, gapuraTransfer.payment)
// .post("/withdrawal/inquiry", allowUser, withdrawal.inquiry)
// .patch("/withdrawal/payment", verifyPayment, withdrawal.payment)
// .patch("/wajib", allowUser, wajib)
// .patch("/wajibPokok", allowUser, wajibPokok)
// .patch("/wajibPokok", allowUser, wajibPokok)
// .patch("/transfer/bank/inquiry", allowUser, bankInquiry)
// .patch("/transfer/bank/payment", allowUser, bankTransfer)

module.exports = route; 