const routes = (app, express, io) => {

  //SOCKET
  require("../middlewares/socketIO")(io)


  // EXPRESS STATIC
  app.use("/clients/ktp", express.static("src/assets/clients/ktp"));
  app.use("/clients/picture", express.static("src/assets/clients/picture"));
  app.use("/clients/signature", express.static("src/assets/clients/signature"));
  app.use("/officers/photo", express.static("src/assets/officers/photo"));
  app.use("/application/collateral", express.static("src/assets/collateral"));

  // ROUTE
  app.use("/area", require("./areas"))
  app.use("/auth/officer", require("./authOfficer"))
  app.use("/auth/client", require("./authClient"))
  app.use("/client/profile", require("./client"))
  app.use("/client/otp", require("./otp"))
  app.use("/client/account", require("./account"))
  app.use("/product", require("./product"))
  app.use("/application", require("./application"))
  app.use("/officer", require("./officer"))

  // app.use("/interest", require("./interest"))
  // app.use("/transaction", require("./transaction"))
  // app.use("/loans", require("./loan"))
  // app.use("/mutation" require("./mutation"))
  // app.use("/savings", require("./deposit"))
  // app.use("/coop", require("./coop")))


  app.get("*", (_, res) => {
    res.status(200).send("Backend is running")
  });

  app.post("*", (_, res) => {
    res.status(400).send("Path not found")
  });

  app.patch("*", (_, res) => {
    res.status(400).send("Path not found")
  });

  app.delete("*", (_, res) => {
    res.status(400).send("Path not found")
  });

}

module.exports = routes;