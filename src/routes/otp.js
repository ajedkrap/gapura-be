const router = require('express').Router()
const OTPCreate = require('../controllers/otp/otpCreate')
const OTPVerify = require('../controllers/otp/otpVerify')

router
  .post("/create", OTPCreate)
  .post("/verify", OTPVerify)

module.exports = router