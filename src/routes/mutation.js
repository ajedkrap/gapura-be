const route = require("express").Router();
const { allowUser } = require("../middlewares/verifyJwt")

route
  .get("/", allowUser, getMutation)

module.exports = route;