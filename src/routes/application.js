
const route = require("express").Router();
const get = require("../controllers/application/getApplication");
const create = require("../controllers/application/createApplication");
const update = require("../controllers/application/updateApplication");
const { allowOperational, withJwt } = require("../middlewares/verifyJwt");

route
  .get("/", get)
  .post("/", withJwt, create)
  .patch("/:appId", allowOperational, update)
// .patch("/patchPath", patchController)
// .delete("/deletePath", deleteController);

module.exports = route;