const route = require("express").Router();
// const {controllerName} = require("[controller-dirname]")
const { allowUser, allowOfficer } = require("../middlewares/verifyJwt")

route
  .get("/", allowOfficer, getAllSavings)
  .get("/:accountId", allowUser, getSavingById)
  .post("/", createSavings)
// .patch("/patchPath", updateLoan)
// .delete("/deletePath", deleteLoan);

module.exports = route;