const route = require('express').Router();
const createProduct = require('../controllers/products/createProduct');
const getProducts = require("../controllers/products/getProducts")
const { allowOperational } = require("../middlewares/verifyJwt")


route
  .get("/", getProducts)
  .post("/:type", allowOperational, createProduct)
// .patch("/:productId, patchController)
// .delete("/:productId", deleteController);

module.exports = route;