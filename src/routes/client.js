const router = require('express').Router()
const get = require("../controllers/client/getClient")
const getActiveClient = require('../controllers/client/getActiveClient')
// const { allowUser } = require("../middlewares/verifyJwt")

router.get("/", get)
router.get("/active", getActiveClient)
// router.patch("/update", )

module.exports = router