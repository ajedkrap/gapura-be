const route = require('express').Router()
const listArea = require("../controllers/areas/listArea")
const getArea = require("../controllers/areas/getArea")

route.get("/list", listArea)
route.get("/get", getArea)

module.exports = route