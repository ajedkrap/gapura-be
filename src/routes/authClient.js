const route = require('express').Router();
const register = require("../controllers/authClient/register");
const login = require("../controllers/authClient/login");

route.post("/register", register);
route.post("/login", login);

module.exports = route
