const route = require("express").Router();
// const {controllerName} = require("[controller-dirname]")
const { allowUser, allowOfficer, verifyPayment } = require("../middlewares/verifyJwt")

route
  // .get("/", allowOfficer, getAllLoans)
  // .get("/:accountId", allowUser, getLoanById)
  .get("/activate/inquiry/:loanId", allowUser, getLoanById)
  .patch("/activate/payment/:loanId", verifyPayment, getLoanById)
// .post("/postPath", createLoan)
// .patch("/patchPath", updateLoan)
// .delete("/deletePath", deleteLoan);

module.exports = route;