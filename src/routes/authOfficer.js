const route = require("express").Router()
const register = require('../controllers/authOfficer/register')
const login = require('../controllers/authOfficer/login')

route
  .post("/register", register)
  .post("/login", login)

module.exports = route

