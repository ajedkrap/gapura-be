const route = require("express").Router();
const get = require("../controllers/officer/getOfficer")

route
  .get("/", get)
// .post("/postPath", postController)
// .patch("/patchPath", patchController)
// .delete("/deletePath", deleteController);

module.exports = route;