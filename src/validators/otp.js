const valid = require("validator")
const { safeString, throw: throwValidator } = require("./validator")
const exists = require("../models/client/clientExists")
const cryptojs = require("crypto-js")
const phoneRegex = /^08[0-9]{9,11}$/g
require('dotenv').config()

const { APP_OTP_KEY: key } = process.env

module.exports = {
  create: async (req) => {
    const phone = safeString(req.phone_number)

    if (
      valid.isEmpty(phone)
    ) return throwValidator(false, "Form Kosong")

    // phone validator
    const isPhone = phone.match(phoneRegex)
    if (!isPhone) return throwValidator(false, "Nomor Handphone tidak Valid")

    else {
      const phoneExists = await exists({ phone_number: phone })
      if (phoneExists) return throwValidator(false, "Nomor Sudah DiRegistrasi")
      else {
        let passed = {
          phone_number: phone
        }
        return throwValidator(true, "OTP Terkirim, Aktif Sampai 5 Menit", passed)
      }
    }
  },
  verify: async (req) => {
    const phone = safeString(req.phone_number)
    const otp = safeString(req.otp)
    const hash = safeString(req.hash)

    if (
      valid.isEmpty(phone) ||
      valid.isEmpty(otp) ||
      valid.isEmpty(hash)
    ) return throwValidator(false, "Form Kosong")

    const [hashValue, expires] = hash.split('.')
    const hashToBytes = cryptojs.AES.decrypt(hashValue, key)
    const decryptedData = JSON.parse(hashToBytes.toString(cryptojs.enc.Utf8))
    const { phone_number: decryptedPhone, otp: decryptedOTP } = decryptedData;

    // phone validator
    const isPhone = phone.match(phoneRegex)
    if (!isPhone) return throwValidator(false, "Nomor Handphone tidak Valid")
    if (phone !== decryptedPhone) return throwValidator(false, "Nomor Handphone tidak sama")

    // opt validator
    if (!otp.length === 6) return throwValidator(false, "OTP tidak Valid")
    if (Date.now() > expires) return throwValidator(false, "OTP Kadaluwarsa")
    if (decryptedOTP !== otp) return throwValidator(false, "OTP Salah")


    else {
      return throwValidator(true, "OTP Terverifikasi")
    }
  }
}