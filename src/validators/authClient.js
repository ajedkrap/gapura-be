const valid = require("validator");
const { throw: throwValidator, safeString } = require("./validator");
const exists = require("../models/client/clientExists");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment')
require('dotenv').config();

const { APP_TOKEN_KEY } = process.env
const phoneRegex = new RegExp(/^08[0-9]{9,11}$/, "g");
const pinRegex = new RegExp(/^\d{6}$/, "g");
const nameRegex = new RegExp(/^[a-zA-Z\s]{2,}$/, "g");

const maritals = ['married', 'single', 'widow']
const genders = ['male', 'female']

const getZeroFill = (id) => {
  return "0".repeat(9 - id.length) + id
}

module.exports = {

  register: async (req) => {
    console.log(req)
    const email = safeString(req.email);
    const phone = safeString(req.phone_number);
    const pin = safeString(req.pin);
    const full_name = safeString(req.full_name);

    const ktp = safeString(req.ktp_number);
    const address = safeString(req.address);
    const areaCode = safeString(req.area_code);
    const marital = safeString(req.marital_status);
    const birthDate = safeString(req.birth_date)
    const birthplace = safeString(req.birthplace)
    const profession = safeString(req.profession)
    const mother = safeString(req.mother_name)
    const gender = safeString(req.gender)

    if (
      valid.isEmpty(email) ||
      valid.isEmpty(phone) ||
      valid.isEmpty(pin) ||
      valid.isEmpty(full_name) ||
      valid.isEmpty(ktp) ||
      valid.isEmpty(address) ||
      valid.isEmpty(areaCode) ||
      valid.isEmpty(birthDate) ||
      valid.isEmpty(birthplace) ||
      valid.isEmpty(profession) ||
      valid.isEmpty(mother) ||
      valid.isEmpty(gender)
    ) return throwValidator(false, "Form Kosong")

    /*   CLIENT   */

    // email validator
    if (!valid.isEmail(email)) return throwValidator(false, "Email Invalid")

    // phone validator
    const isPhone = phone.match(phoneRegex)
    if (!isPhone) return throwValidator(false, "Nomor Handphone tidak Valid")

    // pin validator
    const isCorrectPin = pin.match(pinRegex)
    if (!isCorrectPin) return throwValidator(false, "Pin tidak valid")

    // full name validator
    const isCorrectName = full_name.match(nameRegex)
    if (!isCorrectName) return throwValidator(false, "Nama tidak valid")

    /*   CLIENT_DETAILS   */

    // ktp validator
    if (ktp.length !== 16) return throwValidator(false, "Nomor KTP tidak valid")

    // marital validator
    if (!maritals.includes(marital)) return throwValidator(false, "Status Kawin tidak valid")

    // birthdate validator
    if (!moment(birthDate, "YYYY-MM-DD", true).isValid()) return throwValidator(false, "Tanggal Lahir tidak valid")
    const yearNow = moment().year()
    const yearClient = moment(birthDate).year()
    if ((yearNow - yearClient) < 17) return throwValidator(false, "Anda Belum 17 Tahun")

    // mother_name validator
    const isCorrectName2 = mother.match(nameRegex)
    if (!isCorrectName2) return throwValidator(false, "Nama Ibu Kandung tidak valid")

    // gender validator
    if (!genders.includes(gender)) return throwValidator(false, "Jenis Kelamin tidak valid")

    else {

      const [isExist, payload] = await exists({ phone_number: phone, email, ktp_number: ktp }, "register")
      if (!isExist) return throwValidator(false, payload)
      else {
        let passed = {
          client: {
            id_client: getZeroFill(payload),
            email,
            phone_number: phone,
            pin: bcrypt.hashSync(pin, 12),
            full_name,
          },
          details: {
            address: address.toLowerCase(),
            area_code: areaCode,
            ktp_number: ktp,
            marital_status: marital,
            birthplace,
            birth_date: birthDate,
            profession: profession.toLowerCase(),
            mother_name: mother.toLowerCase(),
            gender
          }
        }

        return throwValidator(true, "Registrasi Berhasil", passed)
      }
    }
  },
  login: async (req) => {
    const phone = safeString(req.phone_number);
    const pin = safeString(req.pin);

    if (
      valid.isEmpty(phone) ||
      valid.isEmpty(pin)
    ) return throwValidator(false, "Form Need To Be Filled")

    // phone validator
    const isPhone = phone.match(phoneRegex)
    if (!isPhone) return throwValidator(false, "Nomor Handphone tidak Valid")

    // pin validator
    const isCorrectPin = pin.match(pinRegex)
    if (!isCorrectPin) return throwValidator(false, "Pin tidak valid")

    else {
      const [isExist, payload] = await exists({ phone_number: phone }, "login");
      if (!isExist) return throwValidator(false, payload)
      else {
        const checkPin = bcrypt.compareSync(pin, payload.pin)
        if (!checkPin) return throwValidator(false, "Pin Salah")
        else {
          delete payload.photo;
          delete payload.created_at;
          delete payload.updated_at;
          const withPin = payload;
          delete payload.pin;
          console.log(withPin)
          const passed = {
            user: payload,
            jwt: jwt.sign({ ...withPin }, APP_TOKEN_KEY)
          }
          return throwValidator(true, "Login Sukses", passed)
        }
      }
    }
  }
}