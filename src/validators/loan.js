require('dotenv').config()
const { throw: throwValidator, safeString } = require("./validator");
const valid = require("validator");
const moment = require('moment');
const jwt = require('jsonwebtoken');
const loanExists = require("../models/loans/getById");
const accountExists = require("../models/account/accountExists");
const { APP_TOKEN_KEY } = process.env

const wajibFee = 5000

module.exports = {
  activate: {
    inquiry: async (req) => {
      const idClient = safeString(req.id_client)
      const loanId = safeString(req.loanId)
      if (valid.isEmpty(loanId)) return throwValidator(false, "Id Pinjaman kosong")
      else {
        const [accExist, accData] = await accountExists({ id_client: idClient })
        if (!accExist) return throwValidator(false, "Akun tidak ditemukan")
        if (accData.status === "inactive") return throwValidator(false, "Akun tidak aktif")
        else {
          const [loanExist, loanData] = await loanExists({ id_client: idClient, id_loan: loanId })
          if (!loanExist) return throwValidator(false, `ID Pinjaman ${loanId} tidak ditemukan`)
          else {
            const { balance, wajib_month, wajib, full_name } = accData
            const { name, tenor } = loanData
            const advanceFee = parseFloat(tenor * wajibFee)
            const newBalance = parseFloat(balance - advanceFee)
            const newWajib = parseFloat(wajib + advanceFee)
            const newWajibMonth = parseFloat(wajib_month + tenor)
            let passed = {
              client: accData,
              loan: loanData,
              nominal: loanData,
              balance,
              new_balance: newBalance,
              wajib,
              new_wajib: newWajib,
              wajib_month,
              new_wajib_month: newWajibMonth,
              nominal: advanceFee
            }

            return (
              true,
              `Inquiry Aktivasi Pinjaman ${name} No. ${loanId} oleh ${full_name} ${idClient}`,
              {
                ...passed,
                jwt: jwt.sign({
                  ...passed,
                  transaction: 'koperasi',
                  date: moment().valueOf()
                }, APP_TOKEN_KEY)
              }
            )
          }
        }
      }
    }
  },
  repayments: {
    inquiry: async (req) => {

    }
  }
}