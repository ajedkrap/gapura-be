const { safeString, throw: throwValidator } = require("./validator")
const areaExists = require("../models/area/getArea")
const moment = require('moment')
const npwpValid = require("../helpers/npwp")

const educations = [
  "SD", "SMP", "SMA", "D1", "D2", "D3", "S1", "S2", "S3",
]
const maritalStatus = [
  'married', 'single', 'widower', 'widow'
]
const residenceStatus = [
  'milik sendiri', 'milik keluarga', 'milik sendiri dijaminkan', 'sewa/kontrak', 'rumah dinas'
]
const religions = [
  'islam', 'kristen', 'katolik', 'hindu', 'buddha', 'konghucu'
]
const genders = [
  'male', 'female'
]


module.exports = async (req) => {

  let passed = {}

  // Update Profile Sanitation & Validation
  for (let key in req) {
    let value = safeString(req[key])
    if (value === "") break
    else {
      switch (key) {
        case 'marital_status':
          value.toLowerCase()
          if (!maritalStatus.includes(value)) return throwValidator(false, "Marital Status Invalid")
          else break
        case 'address':
        case 'birthplace':
        case 'profession':
          value.toLowerCase()
          break
        case 'religion':
          value.toLowerCase()
          if (!religions.includes(value)) return throwValidator(false, "Religion Invalid")
          else break
        case 'residence_status':
          value.toLowerCase()
          if (!residenceStatus.includes(value)) return throwValidator(false, "Residence Status Invalid")
          else break
        case 'gender':
          value.toLowerCase()
          if (!genders.includes(gender)) return throwValidator(false, "Gender Invalid")
          else break
        case 'education':
          value.toLowerCase()
          if (!educations.includes(education)) return throwValidator(false, "Education Invalid")
          else break
        case 'bank_account':
          value.toUpperCase()
          break
        case 'birth_date':
          if (!moment(birth_date).isValid()) return throwValidator(false, "Date Invalid")
          else if (isSameOrAfter) return throwValidator(false, "Date Invalid")
          else if (getAge < 18) return throwValidator(false, "Age Invalid")
          else break
        case 'area_code':
          const isAreaExists = await areaExists(area_code)
          if (typeof isAreaExists !== 'object') return throwValidator(false, "Area Code Invalid")
          else break
        case 'npwp':
          if (npwp.length === 0 || !npwpValid(npwp)) return throwValidator(false, "NPWP Invalid")
          else break
      }
    }
    passed[key] = value
  }

  return throwValidator(true, "Profile Berhasil Diperbaharui", passed)

}
