const valid = require("validator")
const { safeString, throw: throwValidator } = require("./validator")
const productExists = require("../models/product/productExists")

const intCalcs = ['flat', 'effective']
const depoTypes = ['fixed', 'recurring']
const compoundTypes = ['daily', 'monthly']
const postingTypes = ['monthly', 'quarterly', 'annually']
const multiples = ['100', '1000', '10000']
const lockPeriods = ['monthly', 'annually']
const prefixRegex = /^[A-Z]+$/g
const nameRegex = /^[A-Za-z0-9]+$/g

const getType = (type) => type === "depo" ? "Simpanan" : type === "loan" ? "Pinjaman" : new Error('Product Type Error')
const getZeroFill = (increment) => "0".repeat(4 - (increment.toString()).length) + increment.toString()
function myTrim(x) {
  return x.replace(/^\s+|\s+$/gm, '');
}


module.exports = {
  create: async (req) => {
    let product = {}
    let id_product
    const type = safeString(req.type)

    switch (type) {
      case "depo":
        const depoType = safeString(req.deposit_type)
        if (valid.isEmpty(depoType)) return throwValidator(false, "Form Kosong");
        else if (!depoTypes.includes(depoType)) return throwValidator(false, "Tipe Simpanan Tidak Valid");
        else {
          product = { ...product, deposit_type: depoType }
          id_product = `SMPN-${depoType === "fixed" ? "01" : "02"}`
        }
        break;
      case "loan":
        const loanType = safeString(req.with_collateral)
        if (valid.isEmpty(loanType)) return throwValidator(false, "Form Kosong");
        else if (!valid.isBoolean(loanType)) return throwValidator(false, "Tipe Pinjaman Tidak Valid");
        else {
          product = { ...product, with_collateral: parseInt(loanType) }
          id_product = `PNJM-${parseInt(loanType) ? "01" : "02"}`
        }
        break
      default:
        return throwValidator(false, "Tipe Product Tidak Valid");
    }

    let name = safeString(req.name)
    let prefix = safeString(req.prefix)
    let desc = safeString(req.description)
    const intRate = safeString(req.interest_rate)
    const intCalc = safeString(req.interest_calculation)
    const compound = safeString(req.compound)
    const posting = safeString(req.posting)
    const digit = safeString(req["digit_after_decimal"])
    const multiple = safeString(req["in_multiple_of"])
    const lockValue = safeString(req["lock_in_value"])
    const lockPeriod = safeString(req["lock_in_period"])
    const tax = safeString(req.tax)
    const color = safeString(req.theme_color)

    if (
      valid.isEmpty(name) ||
      valid.isEmpty(prefix) ||
      valid.isEmpty(desc) ||
      valid.isEmpty(intRate) ||
      valid.isEmpty(intCalc) ||
      valid.isEmpty(compound) ||
      valid.isEmpty(posting) ||
      valid.isEmpty(digit) ||
      valid.isEmpty(multiple) ||
      valid.isEmpty(lockValue) ||
      valid.isEmpty(lockPeriod) ||
      valid.isEmpty(color)
    ) return throwValidator(false, "Form Kosong");

    // product name validator

    name = myTrim(name).toLowerCase()
    const nameMatch = name.match(nameRegex).length
    if (name.length < 5) return throwValidator(false, `Nama Produk ${getType(type)} Tidak Valid`);
    if (nameMatch < 1) return throwValidator(false, `Nama Produk ${getType(type)} Tidak Valid`);

    // product prefix validator
    prefix = myTrim(prefix)
    const prefixMatch = prefix.match(prefixRegex).length
    if (prefix.length !== 3) return throwValidator(false, "Prefiks Tidak Valid");
    if (prefixMatch < 1) return throwValidator(false, "Prefiks Tidak Valid");

    // description validator
    desc = myTrim(desc)
    if (desc.length < 5) return throwValidator(false, "Deskripsi Produk Tidak Valid");
    if (desc.length > 140) return throwValidator(false, "Deskripsi Produk Tidak Valid");

    // interest rate validator
    if (!valid.isFloat(intRate)) return throwValidator(false, "Bunga Tidak Valid");
    if (!parseFloat(intRate) > 0) return throwValidator(false, "Bunga Tidak Valid");

    // interest calculator validator
    if (!intCalcs.includes(intCalc)) return throwValidator(false, "Perhitungan Bunga Tidak Valid");

    // compound validator
    if (!compoundTypes.includes(compound)) return throwValidator(false, "Compound Tidak Valid");

    // posting validator
    if (!postingTypes.includes(posting)) return throwValidator(false, "Posting Tidak Valid");

    // digit validator
    if (parseInt(digit) < 0) return throwValidator(false, "Digit Setelah Desimal Tidak Valid");
    if (parseInt(digit) > 5) return throwValidator(false, "Digit Setelah Desimal Tidak Valid");

    // multiples validator
    if (!multiples.includes(multiple)) return throwValidator(false, "Kelipatan Uang Tidak Valid");

    // lock in validator
    if (!parseInt(lockValue)) return throwValidator(false, "Nilai Lock In Tidak Valid");
    if (parseInt(lockValue) < 0) return throwValidator(false, "Nilai Lock In Tidak Valid");
    if (!lockPeriods.includes(lockPeriod)) return throwValidator(false, "Periode Lock In Tidak Valid");

    // tax validator
    if (tax.length > 0) {
      for (let taxData of tax) {
        const theTax = taxData.split(" ")
        if (theTax.length !== 2) return throwValidator(false, "Pajak Tidak Valid");
        else {
          const [taxName = taxName.toLowerCase(), taxValue] = theTax
          if (!taxName || !taxName.includes("p") && taxName === "") return throwValidator(false, "Nama Pajak Tidak Valid");
          if (!taxValue || !valid.isFloat(taxValue) || parseFloat(taxValue) < 0) return throwValidator(false, "Nilai Pajak Tidak Valid");
        }
      }
      product = { ...product, tax: tax.toString() }
    }

    // color validator
    if (!valid.isHexColor(color)) return throwValidator(false, "Warna Tema Tidak Valid");

    else {
      const [productExist, payload] = await productExists({ name, prefix }, type)
      if (productExist) return throwValidator(false, `Produk ${getType(type)} ${name}-${prefix} sudah ada`, payload)
      else {
        id_product = id_product + "-" + getZeroFill(payload)
        product = {
          ...product,
          ...type === "depo" ? { id_deposit_prod: id_product } : { id_loan_prod: id_product }
          ,
          name,
          prefix,
          description: desc,
          interest_rate: parseFloat(intRate),
          interest_calculation: intCalc,
          compound,
          posting,
          digit_after_decimal: parseInt(digit),
          in_multiple_of: multiple,
          lock_in_value: parseInt(lockValue),
          lock_in_period: lockPeriod,
          theme_color: color
        }
        return throwValidator(true, `Produk ${getType(type)} Berhasil Dibuat`, product)
      }
    }

  },
  update: async (req) => {
    const { id_product } = req
    if (id_product.includes("SMPN")) type = [{ id_deposit_type: id_product }, "depo"];
    else if (id_product.includes("PNJM")) type = [{ id_loan_type: id_product }, "loan"];
    else return throwValidator(false, "ID Produk tidak valid");
    let updateProduct = {}
    for (let key in req) {
      let updateKey = safeString(req[key])
      if (valid.isEmpty(req[key])) break;
      else {
        switch (key) {
          case "description":
            updateKey = myTrim(updateKey)
            if (updateKey.length < 5 && updateKey.length > 140) return throwValidator(false, "Deskripsi Produk Tidak Valid"); break;
          case "interest_rate":
            if (!valid.isFloat(updateKey) && parseFloat(updateKey) < 0) return throwValidator(false, "Nilai Bunga Tidak Valid");
            updateKey = parseFloat(updateKey);
            break;
          case "interest_calculation":
            if (!intCalcs.includes(updateKey)) return throwValidator(false, "Perhitungan Bunga Tidak Valid"); break;
          case "compound":
            if (!compoundTypes.includes(updateKey)) return throwValidator(false, "Compound Tidak Valid"); break;
          case "posting":
            if (!postingTypes.includes(updateKey)) return throwValidator(false, "Posting Tidak Valid"); break;
          case "digit_after_decimal":
            if (parseInt(updateKey) < 1 && parseInt(updateKey) > 5) return throwValidator(false, "Digit Setelah Desimal Tidak Valid");
            updateKey = parseInt(updateKey)
            break;
          case "in_multiple_of":
            if (!multiples.includes(updateKey)) return throwValidator(false, "Kelipatan Uang Tidak Valid"); break;
          case "lock_in_value":
            if (!parseInt(updateKey) && parseInt(updateKey) < 0) return throwValidator(false, "Nilai Lock In Tidak Valid");
            updateKey = parseInt(updateKey)
            break;
          case "lock_in_period":
            if (!lockPeriods.includes(updateKey)) return throwValidator(false, "Periode Lock In Tidak Valid"); break;
          case "tax":
            if (updateKey.length > 0) {
              for (let taxData of updateKey) {
                const theTax = taxData.split(" ")
                if (theTax.length !== 2) return throwValidator(false, "Pajak Tidak Valid");
                const [taxName, taxValue] = theTax
                if (!taxName || taxName === "") return throwValidator(false, "Nama Pajak Tidak Valid");
                if (!valid.isFloat(taxValue) || parseFloat(taxValue) < 0) return throwValidator(false, "Nilai Pajak Tidak Valid");
              }
              product = { ...product, tax: tax.toString() }
              break;
            }
            else break;
          case "theme_color":
            if (!valid.isHexColor(updateKey)) return throwValidator(false, "Warna Tema Tidak Valid");
            break;
          default:
            return throwValidator(false, "Variable Tidak Valid");
        }
        updateProduct = { ...updateProduct, [key]: updateKey }
      }
    }
    const [productExist, _] = await productExists({ ...type[0] }, type[1])
    if (!productExist) return throwValidator(false, `Produk ${id_product} tidak ditemukan`)
    else return throwValidator(true, `Pembaruan Produk ${getType(type[1])}: ${id_product} Berhasil di update`, updateProduct)
  },
  delete: async (req) => {
    const { id_product } = req; let product;
    if (id_product.includes("SMPN")) product = [{ id_deposit_type: id_product }, "depo"];
    else if (id_product.includes("PNJM")) product = [{ id_loan_type: id_product }, "loan"];
    else return throwValidator(false, "ID Produk tidak valid",);
    const [productExist, data] = await productExists({ ...type[0] }, type[1])
    if (!productExist) return throwValidator(false, `Produk ${id_product} tidak ditemukan`)
    else return throwValidator(true, `Penghapusan Produk ${getType(type[1])}: ${id_product} Berhasil di update`, product)
  }
}