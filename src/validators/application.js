const { safeString, throw: throwValidator } = require("./validator")
const valid = require("validator");
const clientExists = require("../models/client/clientExists");
const accExists = require("../models/account/accountExists");
const getProduct = require("../models/product/getProducts");
const appExists = require("../models/application/appExists")
const appIncrement = require("../models/application/increment");
const colIncrement = require("../models/collateral/increment");
const depoIncrement = require("../models/deposit/increment")
const loanIncrement = require("../models/loans/increment")
const interestCalc = require("../utils/interestCalculation")
const moment = require('moment');

const collaterals = ['vehicle', 'property']
const periods = ['weekly', 'monthly', 'annually']

const getZeroFill = (id) => {
  return "0".repeat(10 - id.length) + id
}


module.exports = {
  create: async (req) => {
    const idClient = safeString(req.id_client)
    const idProd = safeString(req.id_product)
    const name = safeString(req.name)
    const purpose = safeString(req.purpose)
    const amount = safeString(req.amount)
    const tenor = safeString(req.tenor)
    const periodUnit = safeString(req.period_unit)
    const collateral = safeString(req.collateral)
    const spouseData = safeString(req.spouseData)
    const earningData = safeString(req.earningData)

    let appData = {}
    let productData
    let productType
    let colData = {}
    let passed = {}

    if (
      valid.isEmpty(idClient) ||
      valid.isEmpty(idProd) ||
      valid.isEmpty(name) ||
      valid.isEmpty(amount) ||
      valid.isEmpty(tenor) ||
      valid.isEmpty(periodUnit)
    ) return throwValidator(false, "Form Kosong");

    if (idProd.includes("SMPN")) {
      productType = 'depo'
      productData = { id_deposit_prod: idProd }
    }
    else if (idProd.includes("PNJM")) {
      if (valid.isEmpty(purpose)) return throwValidator(false, "Tujuan Pinjaman Kosong");
      productType = 'loan'
      productData = { id_loan_prod: idProd }
    }
    else return throwValidator(false, "Jenis Produk Tidak Valid")

    if (!parseFloat(amount)) return throwValidator(false, "Nilai Besaran Tidak Valid");
    if (!parseInt(tenor)) return throwValidator(false, "Tenor Tidak Valid");
    if (parseInt(tenor) < 1) return throwValidator(false, "Tenor Tidak Valid");
    if (!periods.includes(periodUnit)) return throwValidator(false, "Unit Periode Tidak Valid");

    else {
      const [clientExist, userPayload] = await clientExists({ id_client: idClient })
      if (!clientExist) return throwValidator(false, `Anggota ${idClient} Tidak Ditemukan`)
      else {
        if (userPayload.status === 'inactive') return throwValidator(false, `Anggota ${idClient} Belum Aktif`)
        appData = { ...appData, id_client: idClient }
        const [productExist, prodPayload] = await productExists({ ...productData }, productType)
        if (!productExist) return throwValidator(false, `Produk ${idProd} Tidak Ditemukan`)
        else {
          let message = {}
          appData = { ...appData, id_product: idProd }
          const appInc = await appIncrement()
          const id_application = "APP-" + getZeroFill(appInc.toString())
          appData = {
            ...appData,
            id_application,
            amount: parseFloat(amount),
            tenor,
            period_unit: periodUnit,
            status: 'applied'
          }



          if (productType === 'loan') {

            /* ------ DATA PENDAPATAN ------ */

            if (earningData === "") {
              return throwValidator(false, "Data Pendapatan Tidak ada")
            }
            else {
              for (let key in earningData) {
                const earnKey = safeString(earningData[key])
                if (valid.isEmpty(earnKey)) return throwValidator(false, "Form Data Pendapatan Kosong")
                else break;
              }
              message = { ...message, earningData }
            }

            /* ------ DATA PASANGAN ------ */

            if (spouseData !== "") {
              for (let key in spouseData) {
                const spouKey = safeString(spouseData[key])
                if (valid.isEmpty(spouKey)) return throwValidator(false, "Form Data Pasangan Kosong")
                else break;
              }
              message = { ...message, spouseData }
            }

            appData = {
              ...appData,
              message: JSON.stringify(message)
            }

            /* ------ PINJAMAN AGUNAN ------ */
            if (prodPayload['with_collateral'] === 1) {
              if (collateral === "" || Object.keys(collateral).length < 1) {
                return throwValidator(false, "Agunan Tidak Valid")
              }
              else {
                const { type } = collateral
                let details = {}
                if (!collaterals.includes(type)) return throwValidator(false, "Tipe Agunan Tidak Ditemukan")
                else {
                  const colInc = await colIncrement()
                  const id_collateral = "APP-" + getZeroFill(colInc.toString())
                  switch (type) {
                    case "vehicle":
                      for (let key in collateral) {
                        const colKey = safeString(collateral[key])
                        if (valid.isEmpty(colKey)) return throwValidator(false, "Form Agunan Kosong")
                        else details = { ...details, [key]: colKey }
                      }
                      break;
                    case "property":
                      for (let key in collateral) {
                        const colKey = safeString(collateral[key])
                        if (valid.isEmpty(colKey)) return throwValidator(false, "Form Agunan Kosong")
                        else details = { ...details, [key]: colKey }
                      }
                      break;
                    default:
                      return throwValidator(false, "Tipe Agunan Salah")
                  }
                  colData = {
                    id_application,
                    id_collateral,
                    type,
                    details: JSON.stringify(details)
                  }
                  appData = {
                    ...appData,
                    id_collateral
                  }
                  passed = { ...passed, colData }
                }
              }
              /* ------ PINJAMAN AGUNAN ------ */

            }
          }


          passed = { ...passed, appData }
          return throwValidator(true, `Pengajuan ${idProd.includes("SMPN") ? "Simpanan" : "Pinjaman"} sudah diajukan`, passed)
        }
      }
    }
  },
  progress: async (req) => {
    const idApp = safeString(req['appId'])
    const idOff = safeString(req.id_officer)
    const offName = safeString(req.full_name)

    const [appExist, getApp] = await appExists({ id_application: idApp })
    if (!appExist) return throwValidator(false, `Aplikasi No. ${idApp} tidak ditemukan`)
    else {
      const message = JSON.parse(getApp.message)
      const { approved_by, surveyed_by, reviewed_by, rejected_by } = getApp
      if (rejected_by && rejected_by !== null) return throwValidator(false, `Aplikasi sudah ditolak`)
      if (approved_by && approved_by !== null) return throwValidator(false, `Aplikasi sudah disetujui`)
      if (surveyed_by && surveyed_by !== null) return throwValidator(false, `Aplikasi sedang ditindakLanjuti`)
      if (reviewed_by && reviewed_by !== null) return throwValidator(false, `Aplikasi sudah ditinjau`)
      else {
        newMessage = {
          ...message,
          review: {
            by: idOff,
            name: offName,
            date: moment().toISOString()
          }
        }
        let data = {
          message: JSON.stringify(newMessage),
          reviewed_by: idOff
        }
        return throwValidator(true, `Aplikasi ${idApp} berhasil ditinjau`, [{ ...data }, { id_application: idApp }])
      }
    }
  },
  follow: async (req) => {

    const idApp = safeString(req['appId'])
    const idOff = safeString(req.id_officer)
    const offName = safeString(req.full_name)
    const surveyDate = safeString(req.survey_date)

    if (valid.isEmpty(surveyDate)) return throwValidator(false, "Tanggal Survey Kosong")
    if (valid.isDate(surveyDate, { format: "YYYY-MM-DD" })) return throwValidator(false, "Tanggal Survey tidak valid")

    const [appExist, getApp] = await appExists({ id_application: idApp })
    if (!appExist) return throwValidator(false, `Aplikasi No. ${idApp} tidak ditemukan`)
    else {
      const message = JSON.parse(getApp.message)
      const { approved_by, surveyed_by, reviewed_by, rejected_by } = getApp
      if (rejected_by && rejected_by !== null) return throwValidator(false, `Aplikasi sudah ditolak`)
      if (approved_by && approved_by !== null) return throwValidator(false, `Aplikasi sudah disetujui`)
      if (surveyed_by && surveyed_by !== null) return throwValidator(false, `Aplikasi sedang ditindakLanjuti`)
      if (reviewed_by === null) return throwValidator(false, `Aplikasi belum ditinjau`)
      else {
        newMessage = {
          ...message,
          survey: {
            by: idOff,
            name: offName,
            date: moment().toISOString(),
            survey_date: surveyDate
          }
        }
        let data = {
          message: JSON.stringify(newMessage),
          surveyed_by: idOff,
          survey_date: surveyDate
        }
        return throwValidator(true, `Aplikasi ${idApp} berhasil ditinjau`, [{ ...data }, { id_application: idApp }])
      }
    }


  },
  approval: async (req) => {

    const idApp = safeString(req['appId'])
    const idOff = safeString(req.id_officer)
    const offName = safeString(req.full_name)

    const [appExist, getApp] = await appExists({ id_application: idApp })
    if (!appExist) return throwValidator(false, `Aplikasi No. ${idApp} tidak ditemukan`)
    else {

      const { id_client, name: transName, purpose, amount, tenor, period_unit, id_prod, approved_by, surveyed_by, reviewed_by, rejected_by, message } = getApp

      const [accExist, getAcc] = await accExists({ id_client })
      if (getAcc.status === "inactive") return throwValidator(false, `Anggota No.${id_client} belum aktif`)
      if (!accExist) return throwValidator(false, `Anggota No.${id_client} Tidak ditemukan`)
      else {

        const { full_name: clientName } = getAcc

        if (rejected_by && rejected_by !== null) return throwValidator(false, `Aplikasi sudah ditolak`)
        if (approved_by && approved_by !== null) return throwValidator(false, `Aplikasi sudah disetujui`)
        if (surveyed_by === null) return throwValidator(false, `Aplikasi belum ditindakLanjuti`)
        if (reviewed_by === null) return throwValidator(false, `Aplikasi belum ditinjau`)
        else {

          let transData = {}
          let recordsData = []
          let productType
          let transId
          let approved_date = moment()

          let newMessage = {
            ...JSON.parse(message),
            approve: {
              by: idOff,
              name: offName,
              date: moment(approved_date).toISOString()
            }
          }

          let appData = [
            {
              approved_by: idOff,
              status: "approved",
              message: JSON.stringify(newMessage)
            },
            {
              id_application: idApp
            }
          ]

          const [productExist, prodPayload] = await getProduct({ id: id_prod })
          if (!productExist) return throwValidator(false, `Produk ${id_prod} Tidak Ditemukan`)
          else {
            const { name: prodName, prefix, interest_rate, interest_calculation, digit_after_decimal, tax } = prodPayload
            const getCalculation = await interestCalc(
              {
                interest_rate, interest_calculation, digit_after_decimal, tax: tax.split(',')
              }, {
              amount, tenor, period_unit, start_date: moment(approved_date).add(1, 'd').format("DD-MM-yyyy")
            })


            //CALCULATE INTEREST
            const { repayments, payment } = await getCalculation(productInt, appInt)
            const firstRepayment = repayments[0]

            // SET TRANSACTION + RECORDS
            if (productId.includes("SMPN")) {
              productType = "depo"
              const increment = await depoIncrement()
              transId = prefix + "-" + getZeroFill(increment.toString())
              transData = {
                id_deposit: transId,
                id_depo_prod: productId,
                id_application: idApp,
                id_client,
                name: transName,
                purpose,
                amount,
                tenor,
                period_unit,
                interest: payment.totalInterest,
                tax: payment.totalTax,
                status: 'inactive',
                installment_amount: firstRepayment.installment,
                installment_remaining: parseInt(tenor),
                deposit_remaining: payment.totalAfterTax,
                approved_date: moment(approved_date).format("YYYY-MM-DD"),
                due_date: moment(firstRepayment.due_date, "DD-MM-YYYY").format("YYYY-MM-DD"),
                ...transData
              }
              for (let install of repayments) {
                let records = {
                  id_deposit: transId,
                  tenor: install.tenor,
                  installment_amount: install.installment,
                  penalty: 0,
                  status: "waiting",
                  due_date: install.due_date
                }
                recordsData.push(records)
              }
            } else if (productId.includes("PNJM")) {
              productType = "loan"
              const increment = await loanIncrement()
              transId = prefix + "-" + getZeroFill(increment.toString())
              transData = {
                id_loan: transId,
                id_loan_prod: productId,
                id_application: idApp,
                id_client,
                name: transName,
                purpose,
                amount,
                tenor,
                period_unit,
                interest: payment.totalInterest,
                tax: payment.totalTax,
                status: 'inactive',
                repayment_amount: firstRepayment.installment,
                repayment_remaining: parseInt(tenor),
                loan_remaining: payment.totalAfterTax,
                approved_date: moment(approved_date).format("YYYY-MM-DD"),
                due_date: moment(firstRepayment.due_date, "DD-MM-YYYY").format("YYYY-MM-DD"),
                ...transData
              }
              for (let install of repayments) {
                let records = {
                  id_loan: transId,
                  tenor: install.tenor,
                  repayment_amount: install.installment,
                  penalty: 0,
                  status: "waiting",
                  due_date: install.due_date
                }
                recordsData.push(records)
              }
            }
            else throw new Error('Tipe Transaksi Tidak Valid')


            // const advanceWajibFee = parseFloat(wajibFee) * parseInt(tenor)
            // if (balance > advanceWajibFee) {
            //   const balanceAfterWajib = parseFloat(balance) - parseFloat(advanceWajibFee)
            //   transData = {
            //     ...transData,
            //     status: "on loan"
            //   }
            //   accountData = {
            //     balance: newBalance,
            //     wajib: wajib + parseFloat(advanceWajibFee),
            //     wajib_month: parseInt(wajib_month) + parseInt(tenor)
            //   }
            //   let wajibMutation = {
            //     id_account,
            //     entry: 'credit',
            //     transaction: 'koperasi',
            //     amount: advanceWajibFee,
            //     new_balance: balanceAfterWajib,
            //     note: noteWajib.replace("&", id_client).replace("$", "Rp." + advanceWajibFee).replace("#", tenor)
            //   }
            //   mutationsData.push(wajibMutation)
            //   if (id_prod.includes("PNJM")){
            //     const balanceAfterLoan = parseFloat(balanceAfterWajib) + parseFloat(amount)
            //     let loanMutation = {
            //       id_account,
            //       entry: 'debit',
            //       transaction: 'loans',
            //       amount: amount,
            //       new_balance: balanceAfterLoan,
            //       note: noteLoan.replace("*", prodName).replace("&", transId).replace("$", "Rp." + amount).replace("#", tenor)
            //     }
            //     mutationData.push(loanMutation)
            //   }
            //   passed = { ...passed, accountData}
            // }
            // else {
            //   transData = {
            //     ...transData,
            //     status: "inactive"
            //   }
            // }

            let passed = { type: productType, appData, transData, recordsData, ...passed }

            return throwValidator(
              true,
              `Aplikasi ${idApp} telah setujui, ${productType === "depo" ? "Simpanan" : "Pinjaman"} ${prodName} ${transName} a.n. ${clientName} No. ${transId} telah berhasil dibuat`,
              passed)
          }
        }
      }
    }
  },
  reject: async (req) => {

    const idApp = safeString(req['appId'])
    const idOff = safeString(req.id_officer)
    const offName = safeString(req.full_name)
    const reason = safeString(req.reason)

    reason.trim()
    if (valid.isEmpty(reason)) {
      return throwValidator(false, "Alasan Penolakan tidak boleh kosong")
    }
    if (reason.length < 3) {
      return throwValidator(false, "Alasan Penolakan tidak valid, karakter kurang dari 3")
    }

    const [appExist, getApp] = await appExists({ id_application: idApp })
    if (!appExist) return throwValidator(false, `Aplikasi No. ${idApp} tidak ditemukan`)
    else {
      const { message, rejected_by, approved_by } = getApp

      if (rejected_by && rejected_by !== null) return throwValidator(false, `Aplikasi sudah ditolak`)
      if (approved_by && approved_by !== null) return throwValidator(false, `Aplikasi sudah disetujui`)
      else {

        const newMessage = {
          ...JSON.parse(message),
          reject: {
            by: idOff,
            name: offName,
            date: moment().toISOString,
            reason
          }
        }

        const appData = [
          {
            ...appData,
            message: JSON.stringify(newMessage),
            status: 'reject'
          },
          {
            id_application: idApp
          }
        ]

        return throwValidator(true, `Aplikasi ${idApp} sukses ditolak `, appData)
      }
    }
  }

}