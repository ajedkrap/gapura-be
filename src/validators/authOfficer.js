require('dotenv').config();
const valid = require("validator")
const { throw: throwValidator, safeString } = require("./validator")
const exists = require("../models/officer/officerExists")
const jwt = require("jsonwebtoken")
const bcrypt = require('bcryptjs');
const { APP_TOKEN_KEY } = process.env


const roleTypes = ['operational', 'finance', 'supervisor', 'accountant', 'hr']
const nameRegex = new RegExp(/^[a-zA-Z\s]{2,}$/, 'g')


const getOfficerId = (role, id) => {
  let officerId = "GPR-"
  switch (role) {
    case "supervisor":
      officerId += "1"
      break;
    case "operational":
      officerId += "2"
      break;
    case "accountant":
      officerId += "3"
      break;
    case "finance":
      officerId += "4"
      break;
    case "hr":
      officerId += "5"
      break;
  }
  officerId += "-"
  const length = id.toString().length
  return officerId + "0".repeat(4 - length) + id
}


module.exports = {
  register: async (req) => {
    const alphabetRegex = new RegExp(/[a-zA-Z]+/, 'g')
    const numericRegex = new RegExp(/[0-9]+/, 'g')
    const symbolRegex = new RegExp(/[!@#$%^&*()\-=+_`~<>?,./;':"[\]\\{}|]+/, 'g')

    const email = safeString(req.email)
    const password = safeString(req.password)
    const address = safeString(req.address)
    const full_name = safeString(req.full_name)
    const role = safeString(req.role)

    if (
      valid.isEmpty(email) ||
      valid.isEmpty(password) ||
      valid.isEmpty(full_name) ||
      valid.isEmpty(address) ||
      valid.isEmpty(role)
    ) return throwValidator(false, "Form User Kosong")

    // email validator
    if (!valid.isEmail(email)) return throwValidator(false, "Email tidak valid")

    // password validator
    const alphaValid = alphabetRegex.test(password)
    const numericValid = numericRegex.test(password)
    const symbolValid = symbolRegex.test(password)
    if (!(password.length > 7)) return throwValidator(false, "Password tidak valid, length")
    if (!(alphaValid && numericValid && symbolValid)) return throwValidator(false, "Password tidak valid, AlphaNumericSymbol")

    // full name validator
    const nameValid = full_name.match(nameRegex)
    if (!nameValid) return throwValidator(false, "Nama tidak Invalid")

    // role validator
    if (!roleTypes.includes(role)) return throwValidator(false, "Role tidak Invalid")

    else {
      const [isExist, increment] = await exists({ email })
      if (isExist) return throwValidator(false, "Email sudah terdaftar")
      else {
        let passed = {
          id_officer: getOfficerId(role, increment),
          email,
          password: bcrypt.hashSync(password, 12),
          address,
          full_name: full_name.toLowerCase(),
          role,
        }
        return throwValidator(true, "Officer Berhasil Terdaftar", passed)
      }
    }
  },
  login: async (req) => {
    const email = safeString(req.email);
    const password = safeString(req.password);

    // email validator
    if (!valid.isEmail(email)) return throwValidator(false, "Email tidak valid")

    else {
      const [isExist, data] = await exists({ email })
      if (!isExist) return throwValidator(false, "Email tidak terdaftar")
      if (!bcrypt.compareSync(password, data.password)) return throwValidator(false, "Password Salah")
      else {
        delete data.photo;
        delete data.created_at;
        delete data.updated_at;
        let withPassword = { ...data }
        delete data.password;
        let passed = {
          officer: { ...data },
          jwt: jwt.sign(withPassword, APP_TOKEN_KEY)
        }
        return throwValidator(true, "Officer Login Berhasil", passed)
      }
    }

  }
}