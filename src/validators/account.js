require('dotenv').config()
const { safeString, throw: throwValidator } = require("./validator")
const accExists = require("../models/account/accountExists")
const cliExists = require("../models/client/clientExists")
const jwt = require('jsonwebtoken');
const valid = require("validator");
const moment = require('moment');
const { APP_TOKEN_KEY } = process.env

const wajibFee = 5000
const pokokFee = 20000
const penaltyRate = 1

module.exports = {
  topUp: {
    inquiry: async (req) => {
      const clientId = safeString(req.id_client)
      const nominal = safeString(req.nominal)
      if (
        valid.isEmpty(nominal)
      ) return throwValidator(false, "Form Kosong")

      // nominal validator
      if (parseFloat(nominal) < 10000) return throwValidator(false, 'Nominal kurang dari 10000')

      else {
        const [accountExist, account] = await accExists({ id_client: clientId })
        if (!accountExist) return throwValidator(false, "Akun Tidak Ada")
        else {
          const { balance, id_account } = account
          let passed = {
            receiver: account,
            balance: parseFloat(balance),
            nominal: parseFloat(nominal),
            new_balance: parseFloat(nominal) + parseFloat(balance)
          }
          return throwValidator(
            true,
            `Topup Inquiry ke No.${id_account}`,
            {
              ...passed,
              jwt: jwt.sign({
                ...passed,
                transaction: 'topup',
                date: moment().valueOf()
              }, APP_TOKEN_KEY)
            }
          )
        }
      }
    },
  },
  activate: {
    inquiry: async (req) => {
      const clientId = safeString(req.id_client)
      const tenor = safeString(req.tenor)
      if (
        valid.isEmpty(tenor)
      ) return throwValidator(false, "Form Kosong")

      // nominal validator
      if (parseFloat(tenor) !== 3) return throwValidator(false, 'Nilai tenor tidak valid, 3 Bulan Tenor untuk aktivasi akun')

      else {
        const [accountExist, account] = await accExists({ id_client: clientId })
        if (!accountExist) return throwValidator(false, "Akun Tidak Ada")
        else {
          const { balance, id_account, wajib, pokok, status, wajib_month } = account
          if (status === 'active') return throwValidator(false, "Akun Sudah Aktif")
          else {
            let activateFee = (parseFloat(tenor) * wajibFee) + (pokokFee)
            if (balance < activateFee) return throwValidator(false, "Saldo Simpananku Tidak Cukup")
            else {
              let passed = {
                receiver: account,
                tenor: parseFloat(tenor),
                balance: parseFloat(balance),
                pokok_before: parseFloat(pokok),
                pokok_after: parseFloat(pokokFee),
                wajib_before: parseFloat(wajib),
                wajib_after: parseFloat(tenor) * wajibFee,
                old_wajib_month: parseFloat(wajib_month),
                new_wajib_month: parseFloat(wajib_month) + parseFloat(tenor),
                nominal: parseFloat(activateFee),
                new_balance: parseFloat(balance) - parseFloat(activateFee)
              }
              return throwValidator(
                true,
                `Aktivasi Akun Inquiry ke No.${id_account}`,
                {
                  ...passed,
                  jwt: jwt.sign({
                    ...passed,
                    transaction: 'koperasi',
                    date: moment().valueOf()
                  }, APP_TOKEN_KEY)
                }
              )
            }
          }
        }
      }
    }
  },
  gapuraTransfer: {
    inquiry: async (req) => {
      const clientId = safeString(req.id_client)
      const receiverId = safeString(req.id_receiver)
      const nominal = safeString(req.nominal)
      let message = safeString(req.message)


      if (
        valid.isEmpty(nominal)
      ) return throwValidator(false, "Form Kosong")

      // receiver id validator
      if (receiverId.length !== 9) return throwValidator(false, 'No ID Penerima tidak valid')
      if (clientId === receiverId) return throwValidator(false, 'No ID Penerima tidak boleh sama')

      message.trim()

      // nominal validator
      if (parseFloat(nominal) < 0) return throwValidator(false, 'Nominal tidak valid')

      else {
        const [senderExist, senderAcc] = await accExists({ id_client: clientId })
        if (!senderExist) return throwValidator(false, "Akun Tidak Ada")
        else {
          const [receiverExist, receiverAcc] = await accExists({ id_client: receiverId })
          if (!receiverExist) return throwValidator(false, "Akun Penerima Tidak Ada")
          else {
            const { balance, id_account: senAcc } = senderAcc
            if (parseFloat(balance) < parseFloat(nominal)) return throwValidator(false, "Saldo Simpanaku Tidak Cukup")
            else {
              const { id_account: recAcc, full_name: recName } = receiverAcc
              let passed = {
                sender: senderAcc,
                receiver: {
                  id_client: receiverId,
                  id_account: recAcc,
                  full_name: recName
                },
                balance: parseFloat(balance),
                nominal: parseFloat(nominal),
                new_balance: parseFloat(balance) - parseFloat(nominal)
              }

              if (message !== "") {
                passed = { ...passed, message }
              }

              return throwValidator(
                true,
                `Transfer Inquiry ke ${recName} No.${recAcc} `,
                {
                  ...passed,
                  jwt: jwt.sign({
                    ...passed,
                    transaction: 'transfer',
                    date: moment().valueOf()
                  }, APP_TOKEN_KEY)
                }
              )
            }
          }
        }
      }
    }
  },
  wajib: {
    inquiry: async (req) => {

      let penalty = {}
      const clientId = safeString(req.id_client)
      const tenor = safeString(req.tenor)
      if (
        valid.isEmpty(tenor)
      ) return throwValidator(false, "Form Kosong")

      // nominal validator
      if (!parseInt(tenor) && parseInt(tenor) < 0) return throwValidator(false, 'Nilai tenor tidak valid, 3 Bulan Tenor untuk aktivasi akun')

      else {
        const [accountExist, account] = await accExists({ id_client: clientId })
        if (!accountExist) return throwValidator(false, "Akun Tidak Ada")
        else {
          const { balance, id_account, wajib, status, wajib_month } = account
          if (status === 'inactive') return throwValidator(false, "Akun belum aktif")
          else {
            let passed = {}
            let wajibFee = (parseFloat(tenor) * wajibFee)
            let totalFee = wajibFee

            let wajibDueDate = moment(entry_date).add(wajib_month, 'M').valueOf()
            let currentDate = moment().set({ 'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0 }).valueOf()

            if (currentDate > wajibDueDate) {
              let penaltyMonth = moment(currentDate - wajibDueDate).month() + 1
              penalty = {
                ...penalty,
                wajib_date: moment(wajibDueDate).locale('id').format('dddd, DD-MM-YYYY'),
                inquiry_date: moment(currentDate).locale('id').format('dddd, DD-MM-YYYY'),
                rate: penaltyRate,
                month: penaltyMonth,
                total: (wajibFee * (penaltyRate / 100))
              }
              passed = {
                ...passed,
                penalty,
              }
              totalFee = totalFee + penalty.total
            }

            if (balance < totalFee) return throwValidator(false, "Saldo Simpananku Tidak Cukup", { balance, totalFee })
            else {
              passed = {
                receiver: account,
                tenor: parseFloat(tenor),
                balance: parseFloat(balance),
                wajib_before: parseFloat(wajib),
                wajib_after: wajib + parseFloat(totalFee),
                old_wajib_month: parseFloat(wajib_month),
                new_wajib_month: parseFloat(wajib_month) + parseFloat(tenor),
                nominal: totalFee,
                new_balance: parseFloat(balance) - parseFloat(totalFee),
                ...passed
              }

              return throwValidator(
                true,
                `Simpanan Wajib Inquiry ke No.${id_account}`,
                {
                  ...passed,
                  jwt: jwt.sign({
                    ...passed,
                    transaction: 'koperasi',
                    date: moment().valueOf()
                  }, APP_TOKEN_KEY)
                }
              )
            }
          }
        }
      }
    }
  }
}