require('dotenv').config()
const { Sequelize, DataTypes } = require('sequelize')
const { DB_HOST, DB_USER, DB_PASS, DB_NAME_DEV } = process.env


const seq = new Sequelize(DB_NAME_DEV, DB_USER, DB_PASS, {
  host: DB_HOST,
  dialect: 'mysql',
  pool: {
    min: 0,
    max: 5,
    acquire: 30000,
    idle: 10000,
  },
  define: {
    timestamps: false,
    freezeTableName: true,
    underscored: true
  },
  logging: false,
});

seq.authenticate()
  .then(_ => console.log(`Database ${DB_NAME_DEV} Connected...`))
  .catch(err => new Error(err))



module.exports = { seq, DataTypes }