
const getClientData = require("../controllers/client/getData");
const getCoopData = require("../controllers/coop/getCoopData");
const calculateInterest = require("../controllers/interest/calculateInterest");

module.exports = async (io) => {
  io.on('connection', socket => {
    console.log('socket is on connection')

    socket.on('calculateInterest', async (data, callback) => {
      const interestValue = await calculateInterest(data)
      callback(interestValue)
    })

    socket.on('getClient', async (userId, callback) => {
      const getClient = await getClientData(userId)
      callback(getClient)
    })

    socket.on('getCoopData', async (callback) => {
      const coopData = await getCoopData()
      callback(coopData)
    })

    socket.on('disconnect', () => {
      console.log('a user disconnected');
    })
  });
}