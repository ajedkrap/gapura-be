require('dotenv').config();
const jwt = require("jsonwebtoken");
const moment = require("moment")
const response = require("../helpers/response");
const clientExists = require("../models/client/clientExists");
const officerExists = require("../models/officer/officerExists");
const { APP_TOKEN_KEY } = process.env;

const operationalRole = ['operational', 'supervisor']
const accountingRole = ['accountant', 'supervisor']
const financeRole = ['finance', 'supervisor']
const hrRole = ['hr', 'supervisor']


module.exports = {
  withJwt: async (req, res, next) => {
    const Bearer = req.headers['authorization'] || null;
    if (!Bearer) {
      res.status(403).send(response(false, "Tidak Ada Akses"));
    }
    else {
      const getJwt = jwt.verify(Bearer.replace('Bearer ', ''), APP_TOKEN_KEY);
      const id = getJwt.id || ""
      if (id === "") {
        res.status(401).send(response(false, "Tidak Bisa Akses"));
      }
      else {
        req.payload = { ...getJwt }
        next();
      }
    }
  },
  verifyPayment: async (req, res, next) => {
    const Bearer = req.headers['authorization'] || null;
    if (!Bearer) {
      res.status(403).send(response(false, "Tidak Ada Akses"));
    }
    else {
      const getJwt = jwt.verify(Bearer.replace('Bearer ', ''), APP_TOKEN_KEY);
      const { date } = getJwt
      if (moment(date).add(5, 'm').valueOf() < moment().valueOf()) {
        res.status(400).send(response(false, "Inquiry Expire"));
      } else {
        req.payload = { ...getJwt }
        next();
      }
    }
  },
  allowUser: async (req, res, next) => {
    const Bearer = req.headers['authorization'] || null;
    if (!Bearer) {
      res.status(403).send(response(false, "Tidak Ada Akses"));
    }
    else {
      const getJwt = jwt.verify(Bearer.replace('Bearer ', ''), APP_TOKEN_KEY);
      const id = getJwt.id_client || ""
      if (id === "") {
        res.status(401).send(response(false, "Tidak Bisa Akses"));
      }
      else {
        const [isExist, _] = await clientExists({ id_client: id });
        if (!isExist) {
          res.status(404).send(response(false, "User Tidak Ditemukan"));
        }
        else {
          req.payload = { ...getJwt }
          next();
        }
      }
    }
  },
  allowOfficer: async (req, res, next) => {
    const Bearer = req.headers['authorization'] || null;
    if (!Bearer) {
      res.status(403).send(response(false, "Tidak Ada Akses"));
    }
    else {
      const getJwt = jwt.verify(Bearer.replace('Bearer ', ''), APP_TOKEN_KEY);
      const id_officer = getJwt.id_officer || ""
      if (id_officer !== "") {
        res.status(401).send(response(false, "Tidak Bisa Akses"));
      }
      else {
        const [isExist, _] = await officerExists({ id_officer });
        if (!isExist) {
          res.status(404).send(response(false, "Officer Tidak Ditemukan"));
        }
        else {
          req.payload = { ...getJwt }
          next();
        }
      }
    }
  },
  allowOperational: async (req, res, next) => {
    const Bearer = req.headers['authorization'] || null;
    if (!Bearer) {
      res.status(403).send(response(false, "Tidak Ada Akses"));
    }
    else {
      const getJwt = jwt.verify(Bearer.replace('Bearer ', ''), APP_TOKEN_KEY);
      const role = getJwt.role || "";
      const id_officer = getJwt.id_officer || "";

      if (id_officer === "") {
        res.status(402).send(response(false, "Tidak Bisa Akses"));
      }
      else {
        const [isExist, _] = await officerExists({ id_officer });
        if (!isExist) {
          res.status(404).send(response(false, "Officer Tidak Ditemukan"));
        }
        else {
          if (!operationalRole.includes(role)) {
            res.status(402).send(response(false, "Officer Tidak Bisa Akses"));
          }
          else {
            req.payload = { ...getJwt }
            next();
          }
        }
      }
    }
  }
}