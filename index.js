"use strict"

const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const os = require("os");
const socketIo = require("socket.io");
require('dotenv').config();

const app = express();
const { APP_PORT } = process.env;
const server = require('http').Server(app);
const io = socketIo(server);
const fs = require('fs');
const path = require('path');

// Automatic Simpanan Wajib Payment decalaration
// const autoWajibPayment = require("./src/utils/autoWajibPay")

process.env.NODE_ENV = 'development';

// form-urlencoded
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

// form-data
app.use(bodyParser.json());

// Morgan Log
app.use(morgan('dev'));

app.use(morgan((token, req, res) => {
  return [
    `[${token.date(req, res, 'web')}]`,
    token.method(req, res),
    token.status(req, res),
    token.url(req, res), '-',
    token['response-time'](req, res), 'ms\n',
    `${token['user-agent'](req, res)}\n`,
  ].join(' ')
}, {
  stream: fs.createWriteStream(
    path.join(__dirname, 'access.log'),
    { flags: 'a' })
})
);

// cors
app.use(cors());

// Automatic Simpanan Wajib Payment Function
// autoWajibPayment()


// Router
require('./src/routes/index')(app, express, io);

server.listen(APP_PORT, (err) => {
  if (err) {
    throw err;
  }
  const network = os.networkInterfaces()
  console.log(`IP address: ${network.en0[1].address}`)
  console.log("Express app listening on port " + APP_PORT)
});
